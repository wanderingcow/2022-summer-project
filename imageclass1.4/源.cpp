#include<iostream>
#include"Image.h"

using namespace std;

int main()
{
	Image img1("Fruits.bmp");
	img1.Flip(0);
	img1.WriteBMP("Flip(0).bmp");
	img1.Flip(1);
	img1.WriteBMP("Flip(1).bmp");
	img1.Contrast(200, 121).WriteBMP("Contrast.bmp");
	Image img("Lena.bmp");
	img1.Cat(img, 1).WriteBMP("Cat(1).bmp");
	img1.Cat(img, 2).WriteBMP("Cat(2).bmp");
	img1.Cat(img, 3).WriteBMP("Cat(3).bmp");
	img1.Cat(img, 4).WriteBMP("Cat(4).bmp");
	img.Rotate(270);
	img.WriteBMP("Rotate(270).bmp");
	img1.Resize(800,800);
	img1.WriteBMP("Resize.bmp");
	img1.Cut(0, 0, 200, 200).WriteBMP("Cut.bmp");
	img1.gray2bw(100);
	img1.WriteBMP("gray2bw(100).bmp");
	(-img1).WriteBMP("-.bmp");
	img1.Grayed();
	img1.WriteBMP("Grayed.bmp");
	img.gradient().WriteBMP("gradient.bmp");
	img1.MeanFilter(9).WriteBMP("Mean.bmp");
	Image img2("Fruits.bmp");
	img2.Mosaic(0, 0, 300, 300).WriteBMP("Mosaic.bmp");
	return 0;
}