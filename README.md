# 项目报告：图像处理

**指导老师：** 亓琳

<br>

**小组成员：** 张成玺 杨三瑞 高宇鹏 郝文轩 黄成斌



## 一、绪论
### &ensp; 1.1 问题背景与动机
&ensp; 近年来，人工智能的风暴席卷全球，尤其是基于计算机视觉的创新与革命，充斥着我们的眼球，显得特别高深莫测，其实这些高大上的应用都离不开图像处理的基本算法。经过前人研究表明，人的感觉中，70%是通过视觉感知世界，人眼捕获的图像，经过大脑皮层，神经的处理，最终得到一个我们想得知的结果，反馈给大脑中枢，其实，图像处理系统的工作原理也是有异曲同工之妙。视觉传感器捕获图像，图像处理算法以及神经网络算法等对捕获的图像进行处理，最终结果反馈给中控。图像处理算法是计算机理解图像所必须的环节之一，本次项目主要针对图像处理完成几个基础内容，包括图像翻转、缩放、旋转、拼图、裁剪、灰化、马赛克、滤镜、色彩增强、均值滤波、负片以及图像边缘检测、检测图片中的圆和统计大米面积（像素个数）等功能，选题灵感来源于老师上课内容及课程PPT中展示的功能。
### &ensp; 1.2 相关工作及方法简介
#### &ensp; 1.2.1 相关工作
&ensp; 主要整理了老师上学期所布置作业中完成的功能和代码，并作修改以满足本次功能完成的需要，前期通过网上搜索、查阅资料和书籍结合课程所学内容对本次课程设计的程序做出初步规划，并学习Qt所需相关用法,完成小组分工后开始合作完成本次作业。
#### &ensp; 1.2.2 方法简介
&ensp; 此次项目基于Qt和C++完成程序，对图像处理的方法主要是将`.bmp`图像以数组的方式进行处理，将`.bmp`文件作为Image类对象完成相应操作，图像信息存入一维数组`data`，数组`data`大小为`linebyte = height * width`，通过矩阵变换和矩阵相关算法完成对图像处理的相关操作。
## 二、项目目标
### &ensp; 2.1 程序可视化
&ensp; 通过Qt设计完成程序可视化设计，用户可在可视化界面，通过鼠标和键盘完成对程序的操作，包括读取和保存图片文件，以及完成**绪论**中提到的图像翻转、缩放、旋转、拼图、裁剪、灰化、马赛克、滤镜、色彩增强、均值滤波、负片以及图像边缘检测、检测图片中的圆和统计大米面积（像素个数）等功能的操作。
### &ensp; 2.2 主要功能
#### &ensp; 2.2.1 边缘检测
&ensp; 用户选择传入图片，点击“边缘检测”功能按键，程序输出一张完成边缘检测的灰度图，“边缘检测”主要实现对传入图片中的物体进行边缘检测，主要目的是标识数字图像中亮度变化明显的点。
#### &ensp; 2.2.2 图片中的圆
&ensp; 用户选择传入图片，点击“圆形检测”功能按键，程序输出一张完成圆形检测的图片，在原图基础上，对检测到的圆形进行标记（在圆心用`+`符号标记）
#### &ensp; 2.2.3 统计大米面积
&ensp; 用户选择传入图片，点击“大米检测”功能按键，程序界面输出统计得到的大米面积图像，将检测到的大米以红色标出，并输出大米面积（像素个数）。
### &ensp; 2.3 其他功能
&ensp; 用户通过导航栏“文件(F)”的“打开”选项、图标按键或直接将目标文件拖入窗口的方式，传入并打开文件；对用户传入的图像，用户可通过点击按键或拖动滑块完成对程序的操作，点击“图像翻转”图标，翻转图像；点击“图像旋转”图标，图像逆时针旋转90°；点击“灰度化”按键，图像处理界面展示灰化后的图像；拖动“比例”滑块，完成对图像的缩放，缩放效果在图像处理界面展示，还可通过双击图片，查看大图；滤镜操作栏中包括调节亮度、对比度、饱和度、二值化，通过拖动对应滑块完成相应操作，呈现效果在图像处理窗口展示；色彩增强操作栏，通过拖动“R值增强”、“G值增强”、“B值增强”滑块，完成对相应色彩的增强操作，增强效果同样在图像处理窗口展示；点击“马赛克”按键，可在图像处理窗口中选择打码区域，对图像进行马赛克处理；顶部导航栏点击“拼图(C)”按键选择拼图方式，然后选择要拼接的图像，窗口输出拼接后的图片；点击”裁剪(U)"按键并进行裁剪区域编辑，窗口输出裁剪后的图片；点击“负片”按键，窗口输出经曝光和显影加工后的图像（即负片）。
&ensp; 侧边栏中“显示原图”按键，点击则查看原图，松开鼠标则显示处理后的图片，实现图像处理对比效果；“恢复原图”按键可将处理后的图像还原为原图，显示在图像处理窗口。最后用户可通过导航栏“文件(F)”的“保存”选项或图标按键将操作完成的图片保存到指定位置。

### &ensp; 2.4 展示视频

地址：[项目描述视频](https://www.bilibili.com/video/BV1Kt4y1779H/?vd_source=f308bb72e908166938be253724043f97&t=12.5)

## 三、成员分工
&ensp; 小组共五名成员，张成玺（组长）主要负责完成“检测图片中的圆”功能设计，郝文轩主要负责完成“边缘检测”功能设计、整理修改上学期所完成的代码，杨三瑞主要负责完成GUI界面设计并整合代码完成可视化操作设计，高宇鹏主要负责“统计大米面积”功能设计，黄成斌主要负责项目报告的撰写和项目描述视频制作，成员通过线上讨论互相协助完成程序设计，通过创建`Gitee`仓库提交代码，互相交流，通力合作。

## 四、所用工具和资源
### &ensp; 4.1 所用工具
&ensp; **1、QtCreator** <br>
&ensp; &ensp; 后期实现完整程序，和完成程序可视化设计。<br>
&ensp; **2、Visual Studio** <br>
&ensp; &ensp; 前期还未转入Qt设计时，在Visual Studio先进行相关算法设计和测试。
### &ensp; 4.2 所用资源<br>
&ensp; 1、`<iostream>`<br>
&ensp; 2、`<bits/stdc++.h>`<br>
&ensp; 3、`<Windows.h>`<br>
&ensp; 4、`<QMainWindow>`<br>
&ensp; 5、`<QApplication>`<br>
&ensp; 6、`<Qlacale>`<br>
&ensp; 7、`<QTranslator>`<br>
&ensp; 8、`<QFileDialog>`<br>
&ensp; 9、`<QMessageBox>`<br>
&ensp; 10、`<QLabel>`<br>
&ensp; 11、`<stack>`<br>
&ensp; 12、`<vector>`<br>
&ensp; 13、`<algorithm>`<br>
&ensp; 14、`<math.h>`<br>
&ensp; 15、`<QDragEnterEvent>`<br>
&ensp; 16、`<QMimeData>`<br>
&ensp; 17、`<QIntValidator>`<br>
&ensp; 18、`<QWidget>`<br>

## 五、方法细节
### &ensp; 5.1 边缘检测
&ensp; 边缘检测本质上是一种滤波算法。
#### &ensp; 5.1.1 梯度算子
&ensp; 在边缘检测中，计算梯度是非常重要的一环，梯度可用于检测边缘，一定程度上，在某一点梯度的模越大，可近似的认为该点为边缘点，计算梯度首先要计算偏微分
$${\partial f(x,y) \over \partial x} = lim_{\epsilon \to 0}{{f(x + \epsilon,y)-f(x,y)}\over{\epsilon}}$$
$${\partial f(x,y) \over \partial y} = lim_{\epsilon \to 0}{{f(x,y + \epsilon)-f(x,y)}\over{\epsilon}}$$
&ensp; 梯度
$$G[f(x,y)]=[(\frac {\partial f} {\partial x})^2 + (\frac {\partial f} {\partial y})^2]^{1 \over 2}$$
&ensp; 本项目用Sobel算子
$$ S_x = \begin{bmatrix} -1 & 0 & 1 \\ -2 & 0 & 2 \\ -1 & 0 & 1 \end{bmatrix}$$
$$ S_y = \begin{bmatrix} 1 & 2 & 1 \\ 0 & 0 & 0 \\ -1 & -2 & -1 \end{bmatrix}$$
$$ K = \begin{bmatrix} a_0 & a_1 & a_2 \\ a_7 & [i, j] & a_3 \\ a_6 & a_5 & a_4 \end{bmatrix}$$
&ensp; 分别计算垂直梯度和水平梯度，检测水平边缘与垂直边缘，记录梯度的模。

#### &ensp; 5.1.2 函数实现流程图
```mermaid
graph TB
A(创建对象out用于输出结果)-->B(将图片灰化)-->C(图像数据赋值给out.data)-->D(计算梯度)-->E(确定边缘点)-->F(输出图像)
```
### &ensp; 5.2 图片中的圆
#### &ensp; 5.2.1 算法思路
&ensp; 在极坐标下，假设圆心是$(x_0,y_0)$，则圆上的点可表示为
$$
f(x,y)
\begin{cases}
\ x=x_0 + r\cos{\theta} \\
y = y_0 + r\sin{\theta}
\end{cases}
$$
&ensp; 若已知圆心$(x_0,y_0)$和半径r，那么旋转360°便可求得圆上所有点，同样，若已知圆上所有点，且已知半径r，那么旋转360°便会得到一个累加的极值点，该点即为圆心。
&ensp; 而考虑到圆半径可能未知，所以本项目思路为

```mermaid
graph TB
A(对输入图像边缘检测)-->B(计算图形梯度,并确定圆周)-->C(梯度直线交点累加值越大则越可能是圆心)-->D(统计圆心)
```
#### &ensp; 5.2.2 函数解释
&ensp; 动态创建`bool`类型二维指针`vis`，其大小为`height * width`并将其初始化为`false`，后续用于记录像素点状态。
&ensp; `int cnt = 0, tot = 0`，其中`cnt`用于统计圆的个数，`tot`用于记录像素点累加值。
&ensp; 定义两个数组
```C++
int ex[10] = {-1,0,1,0,-1,1,1,-1,0};
int ey[10] = {0,1,0,-1,1,1,-1,-1,0};
```
用于简化代码，方便进行坐标变换。
```C++
stack<pair<int, int> >st;     
vector<pair<int, int> >g;     //用于记录圆心标记点
```
&ensp; 程序遍历整个图像，确定每个连通域，判断连通域是否为圆，若为圆则`cnt++`，确定圆心并标记，连通域判定条件为
```C++
abs((max_h-min_h) - (max_w-min_w)) <=10 && (max_h-min_h) * (max_w-min_w) >= 200 && (max_h-min_h)<height*0.8 && (max_w-min_w)<width*0.8 && tot>=2500
```
最终输出标记图像。
### &ensp; 5.3 统计大米面积
#### &ensp; 5.3.1 函数实现流程图
```mermaid
graph TB
A(创建对象img)-->B(将图像灰化)-->C(基于灰度图进行阈值分割二值化)-->D(遍历图像基于阈值条件进行筛选)-->E(累计大米面积像素个数)-->F(返回img,输出结果)
```
### &ensp; 5.4 其他功能
#### &ensp; 5.4.1 均值滤波
&ensp; 均值滤波主要采用邻域平均法，其基本原理是用均值代替原图像中各个像素值（m为当前像素及邻近若干像素总个数）
$$g(x,y) = {\sum{f(x,y)} \over m}$$
#### &ensp; 5.4.2 图像翻转
```C++
for (int i = 0; i < height; i++)
{
    int j = 0, k = width - 1;
    while (j < k)
    {
        unsigned char temp = flip[i][j + 2];
        flip[i][j + 2] = flip[i][k];
        flip[i][k] = temp;
        temp = flip[i][j + 1];
        flip[i][j + 1] = flip[i][k - 1];
        flip[i][k - 1] = temp;
        temp = flip[i][j];
        flip[i][j] = flip[i][k - 2];
        flip[i][k - 2] = temp;
        j += 3;
        k -= 3;
    }
}
```
#### &ensp; 5.4.3 图像缩放
```C++
void Image::Resize(int new_height, int new_width)
{
    int i = 0, j = 0;
    unsigned long dwsrcX, dwsrcY;
    unsigned char *pucDest;
    unsigned char *pucSrc;
    unsigned char *dest_data = new unsigned char[new_width * new_height * 3];

    for (i = 0; i < new_height; i++)
    {
        dwsrcY = i * height / new_height;
        pucDest = dest_data + i * new_width * 3;
        pucSrc = data + dwsrcY * width;
        for (j = 0; j < new_width; j++)
        {
            dwsrcX = j * (width / 3) / new_width;
            memcpy(pucDest + j * 3, pucSrc + dwsrcX * 3, 3); //数据拷贝
        }
    }
    delete[] data;
    data = new unsigned char[new_width * 3 * new_height];
    height = new_height;
    width = new_width * 3;
    linebyte = height * width;
    for (int i = 0; i < linebyte; i++)
        data[i] = dest_data[i];
    cout << "放缩成功！" << endl;
    delete[] dest_data;
}
```
#### &ensp; 5.4.4 图像旋转
&ensp; 图像旋转一般认为图像应绕中心点旋转，但是图像原点在左上角，在计算时应将左上角的原点移到图像中心，设一点$(X_0,Y_0)$，图像宽为W，高为H，原点变换后的点为$(X_1,Y_1)$，
$$\begin{bmatrix} X_1 & Y_1 & 1 \end{bmatrix}= \begin{bmatrix} X_0 & Y_0 & 1 \end{bmatrix} \begin{bmatrix} 1 & 0 & 0 \\ 0 & -1 & 0 \\ -0.5W & 0.5H & 1 \end{bmatrix}$$
图像旋转角度为$\theta$，本项目中$\theta$为90°的整数倍，设原点变换后通过矩阵旋转$\theta$后的点为$(X_2,Y_2)$
$$\begin{bmatrix} X_2 & Y_2 & 1 \end{bmatrix} = \begin{bmatrix} X_1 & Y_1 & 1 \end{bmatrix} \begin{bmatrix} \cos{\theta} & -\sin{\theta} & 0 \\ \sin{\theta} & \cos{\theta} & 0 \\ 0 & 0 & 1 \end{bmatrix}$$
旋转后图像的宽为W’，高为H’，从笛卡尔坐标原点变回左上角
$$\begin{bmatrix} X_3 & Y_3 & 1 \end{bmatrix} = \begin{bmatrix} X_2 & Y_2 & 1 \end{bmatrix} \begin{bmatrix} 1 & 0 & 0 \\ 0 & -1 & 0 \\ 0.5W' & 0.5H' & 1 \end{bmatrix} $$

```C++
for (int i = 0, k = width - 1; k > 0; i++, k -= 3)
{
     for (int j = 0; j < height; j++)
    {
        r[i][j * 3] = rotate[j][k - 2];
        r[i][j * 3 + 1] = rotate[j][k - 1];
        r[i][j * 3 + 2] = rotate[j][k];
    }
}
```
#### &ensp; 5.4.5 图像拼接
&ensp; 图像上下拼接，即创建一个新对象cat，图像高为两图像高之和，宽为两图像中较宽之一，依次写入拼接图像与被拼接图像数据
&ensp; 图像左右拼接，同样创建一个新对象cat，图像高为两图像中高较大者，宽为两图像宽之和，分别将拼接图像和被拼接图像数据传入`**a`和`**b`，按拼接要求将`**a`和`**b`赋值给`out`，最终将`out`中数据传给`cat`。
#### &ensp; 5.4.6 图像灰化
&ensp; 本项目中的灰化功能采用平均值法，将RGB三通道的值取平均得到灰度值
```C++
void Image::Grayed()
{
    for (int i = 0; i < height; i++)
        for (int j = 0; j < width; j += 3)
        {
            unsigned char temp = (data[i * width + j + 2] + data[i * width + j + 1] + data[i * width + j]) / 3;
            data[i * width + j] = temp;
            data[i * width + j + 1] = temp;
            data[i * width + j + 2] = temp;
        }
}
```
#### &ensp; 5.4.7 马赛克
&ensp; 用户选定打码范围后，函数获得起始和终止像素点坐标，程序设定马赛克色块大小`x=10`，传入参数，程序选择色块初始点，以初始点色彩数值替换该色块其他像素点，达到降低图片分辨率的效果。
```C++
for (int i = x1; i < x2 - x; i += x)
{
    for (int m = 0; m < x; m++)
    {    
        for (int j = y1 * 3; j < y2 * 3 - x * 3; j += x * 3)
        {
            unsigned char t1 = Mos[i][j], t2 = Mos[i][j + 1], t3 = Mos[i][j + 2];
            for (int n = 0; n < x; n++)
            {
                Mos[i + m][j + n * 3] = t1;
                Mos[i + m][j + n * 3 + 1] = t2;
                Mos[i + m][j + n * 3 + 2] = t3;
            }
        }
    }
}
```
#### &ensp; 5.4.8 图像滤镜
##### &ensp; 5.4.8.1 亮度
&ensp; 对每个像素点的色彩值进行运算，在保证值的范围为[0, 255]的前提下，将像素点的值加上value,即完成亮度调整，若值高于255则取255，低于0则取0。
##### &ensp; 5.4.8.2 对比度
&ensp; 对比度是不同像素点之间的差值，差值越大，对比度越强。
&ensp; 判断`contrast`正负，若`contrast > 0`则
```C++
double x = check(data[i] + (data[i] - thres) * (1 / (1 - contrast / 255.0) - 1));
Con.data[i] = x;
```
若`contrast < 0`则
```C++
double x = check(data[i] + (data[i] - thres) * contrast / 255.0);
Con.data[i] = x;
```
若`contrast == 0`则
```C++
Con.data[i] = data[i];
```
##### &ensp; 5.4.8.3 饱和度
&ensp; 调整饱和度首先将RGB空间图像转换为HSL空间
$$L = {\frac 1 2}(max + min)$$
$$
S =
\begin{cases}
{\frac{max - min} {max + min}} &if\ L \le {\frac 1 2} \\ \\
{\frac {max - min} {2 - (max + min)}} &if\ L \ge {\frac 1 2}
\end{cases}
$$
其中L表示亮度，S表示饱和度，min和max代表RGB空间中的R、G、B颜色值中的最小最大值，范围均为[0, 1]的实数
```C++
minVal = min(min(nRed, nGreen), nBlue);
maxVal = max(max(nRed, nGreen), nBlue);
delta = (maxVal - minVal) / 255.0;
L = 0.5 * (maxVal + minVal) / 255.0;
S = max(0.5 * delta / L, 0.5 * delta / (1 - L));
```
##### &ensp; 5.4.8.4 二值化
&ensp; 对用户传入图像以及所给阈值，小于阈值取0，大于阈值取255
```C++
data[i] = data[i] < (unsigned char)t ? 0 : 1;
data[i] *= 255;
```
#### &ensp; 5.4.9 图像色彩增强
&ensp; 用户选择R、G、B任一色彩值，根据增强值value对每一像素点的相应色彩值进行运算，保证色彩值范围在[0, 255],高于255则取255，低于0则取0。

#### &ensp; 5.4.10 图像裁剪

&ensp; 用户定位裁剪区域的坐标点，建立新的对象存储选定区域的像素内存数据







## 六、结果
### 6.1 初始界面
![初始界面](result/%E5%88%9D%E5%A7%8B%E7%95%8C%E9%9D%A2.png)

打开并显示位图文件：

![bmp](result/Open-Show.png)

### 6.2 边缘检测
![边缘检测](result/%E8%BE%B9%E7%BC%98%E6%A3%80%E6%B5%8B.png)
### 6.3 圆形检测
原图：

![circle](result/Circle.png)

检测结果：

![圆形检测](result/Circle_result.png)

### 6.4 大米检测

原图：

![rice](result/Rice.png)

检测结果：

![大米检测](result/%E5%A4%A7%E7%B1%B3%E6%A3%80%E6%B5%8B.png)
### 6.5 均值滤波
![均值滤波](result/%E5%9D%87%E5%80%BC%E6%BB%A4%E6%B3%A2.png)
### 6.6 图像翻转
![图像翻转](result/%E7%BF%BB%E8%BD%AC.png)
### 6.7 图像缩放
#### 6.7.1 图像放大
![图像放大](result/%E6%94%BE%E5%A4%A7.png)
#### 6.7.2 图像缩小
![图像缩小](result/%E7%BC%A9%E5%B0%8F.png)
### 6.8 图像旋转
#### 6.8.1 旋转90°
![旋转90°](result/result/%E6%97%8B%E8%BD%AC90.png)
#### 6.8.2 旋转180°
![旋转180°](result/result/%E6%97%8B%E8%BD%AC180.png)
### 6.9 图像拼接
上拼效果：

![上拼](result/Cat-Up.png)

双击放大：

![双击放大](result/Doubleclick.png)

左拼效果：

![左拼](result/Cat-Left.png)

### 6.10 图像灰化
![图像灰化](result/%E7%81%B0%E5%8C%96.png)
### 6.11 马赛克
#### 6.11.1 马赛克设置
![马赛克设置](result/%E9%A9%AC%E8%B5%9B%E5%85%8B%E8%AE%BE%E7%BD%AE.png)
#### 6.11.2 马赛克
![马赛克](result/%E9%A9%AC%E8%B5%9B%E5%85%8B.png)
### 6.12 图像滤镜
#### 6.12.1 亮度
##### 6.12.1.1 亮度加
![亮度加](result/%E4%BA%AE.png)
##### 6.12.1.2 亮度减
![亮度减](result/%E6%9A%97.png)
#### 6.12.2 对比度
##### 6.12.2.1 对比度增
![对比度增](result/%E5%AF%B9%E6%AF%94%E5%BA%A6%E5%A2%9E.png)
##### 6.12.2.2 对比度减
![对比度减](result/%E5%AF%B9%E6%AF%94%E5%BA%A6%E5%87%8F.png)
#### 6.12.3 饱和度
![饱和度](result/result/%E9%A5%B1%E5%92%8C%E5%BA%A6.png)
#### 6.12.4 二值化
![二值化](result/%E4%BA%8C%E5%80%BC%E5%8C%96.png)
### 6.13 色彩增强
#### 6.13.1 R值增强
![R值增强](result/R%E5%80%BC%E5%A2%9E%E5%BC%BA.png)
#### 6.13.2 G值增强
![G值增强](result/G%E5%80%BC%E5%A2%9E%E5%BC%BA.png)
#### 6.13.3 B值增强
![B值增强](result/B%E5%80%BC%E5%A2%9E%E5%BC%BA.png)



### 6.14 裁剪

#### 6.14.1 裁剪编辑

![裁剪编辑](result/裁剪编辑.png)

#### 6.14.2 裁剪

![裁剪](result/裁剪.png)

### 6.15 负片

![负片](result/负片.png)

### 6.16 恢复原图

![恢复](result/Recover.png)

## 七、总结与讨论
&ensp; 本次项目由五人小组分工合作完成，完成小组分工后，互相帮助，合作完成，各自付出了努力，也收获了许多。从程序设计的开始，确定了选题，然后各自编写代码，中途也遇到了不少问题，可能思路中断，又或者调试发现许多BUG，小组成员都会在群里讨论，并解决问题，这也确实让我们感受到了队友与合作的重要性，正确的合作方式常常能够事半功倍，当然，有时候我们也会焦急如焚，在程序完成的后期，程序仍存在一些BUG，在担心进度的时候，心里不由得紧张起来，好在最后还是如期完成了项目，这也多亏了有这么一群好队友，才能让项目圆满完成。总之，这次的项目，让小组的每位成员都收获满满，不仅是专业上的能力，还是小组沟通交流，又或是组员之间的情谊，也感谢老师的悉心教导，和助教的帮助，让我们的学习更进一步。

## 八、贡献声明

### &ensp; 8.1 个人贡献（上传日志）
8/19 **郝文轩**上传了图像与矩阵类代码文件：

* 包括：读写、缩放、旋转、翻转、负片、二值化、裁剪等算法

8/22 **张成玺**上传了圆形搜索代码文件

8/23 **郝文轩**上传了边缘检测代码文件

8/23 **张成玺**更新圆形计数和圆心标记算法

8/23 **郝文轩**更新了imageclass版本1.1：

* 修改了Image的继承并删除了Matrix类
* 将位图修改为一维数组存储
* 新增了位图拼接算法
* 更新了边缘检测算法

8/24 **杨三瑞**上传了GUI

* 增加使用QImage显示Image类

8/24 **郝文轩**更新了imageclass版本1.2：

* 修复了颜色问题

8/27 **张成玺**为圆形检测算法 添加unsigned char一维数组类型 适配

8/29 **郝文轩**上传了imageclass版本1.4：

* 修复了resize算法的bug
* 新增了对比度调整算法
* 新增了马赛克算法

8/29 **高宇鹏**上传了大米搜索算法

8/30 **杨三瑞**上传了GUI版本2.0：

* 完善图形界面，增加图标
* 对接对比度，边缘检测算法

8/31 **张成玺**更新GUI版本2.1：

* 添加圆形检测算法
* 实现亮度调节功能
* 实现色彩增强功能

8/31 **杨三瑞**上传了GUI版本3.0：

* 完善图形界面
* 对接大米检测，马赛克，负片
* 实现饱和度调节

8/31 **郝文轩**更新了GUI：

* 对接拼图算法
* 实现双击放大显示
* 实现拖拽打开文件
* 设置窗口图标
* 对接裁剪算法

## 九、引用参考

[QT Labe中l实现双击全屏显示画面 - 灰信网](https://www.freesion.com/article/199972653/)

[Qt：拖拽图片到QLabel上并显示 - 逛奔的蜗牛 - C++博客](http://www.cppblog.com/biao/archive/2011/10/23/158940.html)

[【数字图像处理】C++读取、旋转和保存bmp图像文件编程实现 - 百度文库](https://wenku.baidu.com/view/9623b65e1411cc7931b765ce050876323112740d.html)

[C/C++图像处理4 边缘检测_诺有缸的高飞鸟的博客-CSDN博客_c++图像边缘检测](https://blog.csdn.net/qq_41102371/article/details/107188712)