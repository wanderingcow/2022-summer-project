#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "imageclass1.4/Image.h"
#include <QLabel>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void Reset();

    QImage AdjustImageSize(QImage qimage,QLabel *qLabel);

    void ShowImage(Image &img);

    bool CheckImageFail();

    void on_action_Open_triggered();

    void SaveImage();

    void on_pushButton_save_clicked();

    void on_action_Save_triggered();

    void on_action_UpCat_triggered();

    void on_pushButton_gray_clicked();

    void on_pushButton_origin_released();

    void on_pushButton_origin_pressed();

    void on_pushButton_turnleft_clicked();

    void on_pushButton_flip_clicked();

    void on_pushButton_recover_clicked();

    void on_pushButton_junzhi_clicked();

    void on_pushButton_gradient_clicked();

    void on_pushButton_yuanxing_clicked();

    void on_horizontalSlider_erzhi_valueChanged(int value);

    void on_horizontalSlider_erzhi_sliderReleased();

    void on_horizontalSlider_duibi_valueChanged(int value);

    void on_horizontalSlider_duibi_sliderReleased();

    void on_horizontalSlider_2_valueChanged(int value);

    void on_horizontalSlider_2_sliderReleased();

    void on_action_meanFliter_triggered();

    void on_action_gradient_triggered();

    void on_action_yuanxing_clicked_triggered();

    void on_pushButton_rice_clicked();

    void on_horizontalSlider_R_valueChanged(int value);

    void on_horizontalSlider_R_sliderReleased();

    void on_horizontalSlider_G_valueChanged(int value);

    void on_horizontalSlider_G_sliderReleased();

    void on_horizontalSlider_B_valueChanged(int value);

    void on_horizontalSlider_B_sliderReleased();

    void on_horizontalSlider_brightness_valueChanged(int value);

  //  void on_horizontalSlider_valueChanged(int value);

  //  void on_horizontalSlider_baohe_valueChanged(int value);

    void on_pushButton_masaike_2_clicked();

    bool eventFilter(QObject *watched, QEvent *event);


private:
    Ui::MainWindow *ui;
    Image srcImage;
    Image curImage;
    QString filePath;
    int lastSlideVal=0;

    int lastR = 0;
    int lastG = 0;
    int lastB = 0;
};
#endif // MAINWINDOW_H
