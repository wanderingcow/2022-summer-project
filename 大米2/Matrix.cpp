#define _CRT_SECURE_NO_WARNINGS
#include "Matrix.h"
#include<iostream>
#include<Windows.h>

using namespace std;


Matrix::Matrix()
{
	cout << "调用默认构造函数成功" << endl;
}
Matrix::Matrix(int h, int w)
{
	height = h;
	width = w;
	data = new double* [height];
	for (int i = 0;i < height;i++)
		data[i] = new double[width];
	for (int i = 0;i < height;i++)
		for (int j = 0;j < width;j++)
			data[i][j] = 0;
	cout << "调用构造函数成功" << endl;
}
Matrix::Matrix(int h, int w, double val)
{
	height = h;
	width = w;
	data = new double* [height];
	for (int i = 0;i < height;i++)
		data[i] = new double[width];
	for (int i = 0;i < height;i++)
		for (int j = 0;j < width;j++)
			data[i][j] = val;
	cout << "调用构造函数成功" << endl;
}
Matrix::Matrix(const Matrix& m)
{
	height = m.height;
	width = m.width;
	data = new double* [height];
	for (int i = 0;i < height;i++)
		data[i] = new double[width];
	for (int i = 0;i < height;i++)
		for (int j = 0;j < width;j++)
			data[i][j] = m.data[i][j];
	cout << "调用构造函数成功" << endl;
}
Matrix::~Matrix()
{
	cout << "Matrix:已调用析构函数" << endl;
}

void Matrix::ReadText(const char* filename)//从文本文件中读入矩阵数据;
{
	FILE* fp = fopen(filename, "r");
	if (!fp)
	{
		cout << "Matrix:Error!" << endl;
		exit(0);
	}

	data = new double* [height];
	for (int i = 0;i < height;i++)
		data[i] = new double[width * 3];
	for (int i = 0;i < height;i++)
		for (int j = 0;j < width * 3;j++)
			fread(&data[i][j], 1, 1, fp);
	cout << "矩阵读入成功" << endl;
	fclose(fp);
}
void Matrix::WriteText(const char* filename)//将矩阵数据保存为文本文件;
{
	FILE* wfp = fopen(filename, "w");
	if (!wfp)
	{
		cout << "Matrix:文件无法创建！" << endl;
		exit(0);
	}
	for (int i = 0;i < height;i++)
		fwrite(data[i], 1, width, wfp);
	fclose(wfp);
	cout << "矩阵写入成功！" << endl;
}
void Matrix::Zeros(int h, int w)// 根据参数产生h行w列的全零矩阵
{
	height = h;
	width = w;
	for (int i = 0;i < height;i++)
		delete[]data[i];
	delete[]data;
	data = new double* [height];
	for (int i = 0;i < height;i++)
		data[i] = new double[width];
	for (int i = 0;i < h;i++)
		for (int j = 0;j < width;j++)
			data[i][j] = 0;
	cout << "矩阵产生成功:0" << endl;
}
void Matrix::Ones(int h, int w)// 根据参数产生h行w列的全1矩阵
{
	height = h;
	width = w;
	for (int i = 0;i < height;i++)
		delete[]data[i];
	delete[]data;
	data = new double* [height];
	for (int i = 0;i < height;i++)
		data[i] = new double[width];
	for (int i = 0;i < h;i++)
		for (int j = 0;j < width;j++)
			data[i][j] = 1;
	cout << "矩阵产生成功:1" << endl;
}
void Matrix::Random(int h, int w)//产生h行w列的随机矩阵，矩阵的元素为[0,1]之间的随机实数（double类型）
{
	height = h;
	width = w;
	for (int i = 0;i < height;i++)
		delete[]data[i];
	delete[]data;
	data = new double* [height];
	for (int i = 0;i < height;i++)
		data[i] = new double[width];
	for (int i = 0;i < h;i++)
		for (int j = 0;j < w;j++)
			data[i][j] = rand()%1000/(double)1000;
	cout << "矩阵产生成功:random" << endl;
}
void Matrix::Identity(int n)// 根据参数产生n行n列的单位矩阵
{
	height = n;
	width = n;
	for (int i = 0;i < height;i++)
		delete[]data[i];
	delete[]data;
	data = new double* [height];
	for (int i = 0;i < height;i++)
		data[i] = new double[width];
	for (int i = 0;i < height;i++)
		for (int j = 0;j < width;j++)
			data[i][j] = 0;
	cout << "矩阵产生成功:n*n" << endl;
}

int Matrix::Height() const// 获得矩阵的行数
{
	return height;
}
int Matrix::Width() const// 获得矩阵的列数
{
	return width;
}
Matrix Matrix::MajorDiagonal()// 求矩阵主对角线上的元素，输出一个N行1列的矩阵，N为主对角线元素的个数
{
	int n = height < width ? height : width;
	Matrix major(n,1);
	for (int i = 0;i < n;i++)
		major.data[i][0] = data[i][i];
	cout << "主对角线元素为：" << endl;
	for (int i = 0;i < n;i++)
		cout << major.data[i][0]<<endl;
	return major;
}
Matrix Matrix::MinorDiagonal()// 求矩阵的副对角线上的元素，输出一个N行1列的矩阵，N为副对角线上元素的个数
{
	int n = height < width ? height : width;
	Matrix minor(n, 1);
	for (int i = 0;i < n;i++)
		minor.data[i][0] = data[i][n-1-i];
	cout << "主对角线元素为：" << endl;
	for (int i = 0;i < n;i)
		cout << minor.data[i][0] << endl;
	return minor;
}
Matrix Matrix::Row(int n)// 返回矩阵的第n行上的元素，组出一个1行N列的矩阵输出，N为第n行上元素的个数
{
	Matrix row(1, width);
	for (int i = 0;i < width;i++)
		row.data[0][i] = data[n][i];
	cout << "第n行元素为：" << endl;
	for (int i = 0;i < width;i++)
		cout << row.data[0][i] << " ";
	return row;
}
Matrix Matrix::Column(int n)// 返回矩阵的第n列上的元素，组出一个N行1列的矩阵输出，N为第n列上元素的个数
{
	Matrix column(height,1);
	for (int i = 0;i < height;i++)
		column.data[i][0] = data[i][n];
	cout << "第n行元素为：" << endl;
	for (int i = 0;i < height;i)
		cout << column.data[i][0] << endl;
	return column;
}

void Matrix::Transpose()// 将矩阵转置
{
	Matrix trans(width, height);
	for (int i = 0;i < width;i++)
		for (int j = 0;j < height;j++)
			trans.data[i][j] = data[j][i];
	for (int i = 0;i < height;i++)
		delete[]data[i];
	delete[]data;
	double temp;
	temp = height;
	height = width;
	width = temp;
	data = new double* [height];
	for (int i = 0;i < height;i++)
		data[i] = new double[width];
	for (int i = 0;i < height;i++)
		for (int j = 0;j < width;j++)
			data[i][j] = trans.data[i][j];
	cout << "矩阵转置成功！" << endl;

}

double& Matrix::At(int row, int col) const//获取第row行第col列的矩阵元素的值
{
	return data[row][col];
}
void Matrix::Set(int row, int col, double value)//设置第row行第col列矩阵元素的值为value
{
	data[row][col] = value;
}
void Matrix::Set(double value)//设置矩阵所有元素为同一值value
{
	for (int i = 0;i < height;i++)
		for (int j = 0;j < width;j++)
			data[i][j] = value;
	cout << "矩阵元素设定成功！" << endl;
}

void Matrix::Reshape(int h, int w)//在矩阵元素总数不变的情况下，将矩阵行列变为参数给定的大小
{
	double *reshape = new double[width * height];
	int n=0;
	for(int i=0;i<height;i++)
		for (int j = 0;j < width;j++)
		{
			reshape[n] = data[i][j];
			n++;
		}
	for (int i = 0;i < height;i++)
		delete[]data[i];
	delete[]data;
	height = h;
	width = w;
	n = 0;
	data = new double* [height];
	for (int i = 0;i < height;i++)
		data[i] = new double[width];
	for(int i=0;i<h;i++)
		for(int j=0;j<w;j++)
		{
			data[i][j] = reshape[n];
			n++;
		}
	delete[]reshape;
	cout << "矩阵长宽修改成功！" << endl;
}
bool Matrix::IsEmpty()// 判断矩阵是否为空矩阵
{
	return data == NULL ? true : false;
}
bool Matrix::IsSquare()// 判断矩阵是否为方阵
{
	return height == width ? true : false;
}
void Matrix::CopyTo(Matrix& m) // 将矩阵复制给m
{
	for (int i = 0;i < height;i++)
		delete[]data[i];
	delete[]data;
	height = m.height;
	width = m.width;
	data = new double* [height];
	for (int i = 0;i < height;i++)
		data[i] = new double[width];
	for (int i = 0;i < height;i++)
		for (int j = 0;j < width;j++)
			data[i][j] = m.data[i][j];
	cout << "矩阵复制成功" << endl;
}
void Matrix::Mult(double s) // 矩阵的每个元素都乘以参数s
{
	for (int i = 0;i < height;i++)
		for (int j = 0;j < width;j++)
			data[i][j] *= s;
	cout << "矩阵元素修改成功！" << endl;
}
void Matrix::Cat(Matrix& m, int code) // 将矩阵m与当前矩阵进行拼接，code代表拼接的方式：将m拼接到当前矩阵的上、下、左、右，具体例子见本大纲后面的说明
{
	switch (code)
	{
	case 1: //下
	{
		int w = m.width < width ? m.width : width;
		double** cat = new double* [height + m.height];
		for (int i = 0;i < height + m.height;i++)
			cat[i] = new double[w];
		int i = 0, x = 0,y=0;
		for (i, x;i < height + m.height;i++, x++)
		{
			if (x < m.height)
				for (int j  =0 ;j < w; j++)
					cat[i][j] = m.data[x][j];
			else
			{
				for (int j = 0;j < w; j++)
					cat[i][j] = this->data[y][j];
				y++;
			}
	    }
		cout << "矩阵上接成功！" << endl;
		width = w;
		height = height + m.height;
		data = new double* [height];
		for (int i = 0;i < height;i++)
			data[i] = new double[width];
		for (int i = 0;i < height;i++)
			for (int j = 0;j < width;j++)
				data[i][j] = cat[i][j];
		break;
	}
	case 2: //上
	{
		int w = m.width < width ? m.width : width;
		double** cat = new double* [height + m.height];
		for (int i = 0;i < height + m.height;i++)
			cat[i] = new double[w];
		int i = 0, x = 0, y = 0;
		for (i, x;i < height + m.height;i++, x++)
		{
			if (x < height)
				for (int j = 0;j < w; j++)
					cat[i][j] = this->data[x][j];
			else
			{
				for (int j = 0;j < w; j++)
					cat[i][j] = m.data[y][j];
				y++;
			}
		}
		cout << "矩阵下接成功！" << endl;
		for (int i = 0;i < height;i++)
			delete[]data[i];
		delete[]data;

		width = w;
		height = height + m.height;
		data = new double* [height];
		for (int i = 0;i < height;i++)
			data[i] = new double[width];
		for (int i = 0;i < height;i++)
			for (int j = 0;j < width;j++)
				data[i][j] = cat[i][j];
		break;
	}
	case 3: //左
	{
		int h = m.height < height ? m.height : height;
		double** cat = new double* [h];
		for (int i = 0;i < h;i++)
			cat[i] = new double[width+m.width];
		for (int i = 0;i < h;i++)
		{
			int j = 0, x=0,y = 0;
			for (j, y;j < width + m.width; j++, y++)
			{
				if(y<m.width)
				    cat[i][j] = m.data[i][y];
				else
				{
					cat[i][j] = this->data[i][x];
					x++;
				}
			}
		}
		cout << "矩阵左接成功！" << endl;
		width = width+m.width;
		height = h;
		data = new double* [height];
		for (int i = 0;i < height;i++)
			data[i] = new double[width];
		for (int i = 0;i < height;i++)
			for (int j = 0;j < width;j++)
				data[i][j] = cat[i][j];
		break;
	}
	case 4: //右
	{
		int h = m.height < height ? m.height : height;
		double** cat = new double* [h];
		for (int i = 0;i < h;i++)
			cat[i] = new double[width + m.width];
		for (int i = 0;i < h;i++)
		{
			int j = 0, x = 0,y = 0;
			for (j, y;j < width + m.width; j++, y++)
			{
				if (y < width)
					cat[i][j] = this->data[i][y];
				else
				{
					cat[i][j] = m.data[i][x];
					x++;
				}
			}
		}
		cout << "矩阵右接成功！" << endl;
		width = width + m.width;
		height = h;
		data = new double* [height];
		for (int i = 0;i < height;i++)
			data[i] = new double[width];
		for (int i = 0;i < height;i++)
			for (int j = 0;j < width;j++)
				data[i][j] = cat[i][j];
		break;
	}
	default:
		cout << "矩阵拼接code错误！" << endl;
		break;
	}

}

Matrix Add(const Matrix& m1, const Matrix& m2)// 友元函数，将矩阵m1和m2相加，结果矩阵作为函数的返回值
{
	int h = m1.height < m2.height ? m1.height : m2.height;
	int w = m1.width < m2.width ? m1.width : m2.width;
	Matrix add(h, w);
	for (int i = 0;i < h;i++)
		for (int j = 0;j < w;j++)
			add.data[i][j] = (m1.data[i][j] + m2.data[i][j])>255?255: (m1.data[i][j] + m2.data[i][j]);
	return add;
}
Matrix Sub(const Matrix& m1, const Matrix& m2) // 友元函数，将矩阵m1和m2相减，结果矩阵作为函数的返回值
{
	int h = m1.height < m2.height ? m1.height : m2.height;
	int w = m1.width < m2.width ? m1.width : m2.width;
	Matrix sub(h, w);
	for (int i = 0;i < h;i++)
		for (int j = 0;j < w;j++)
			sub.data[i][j] = (m1.data[i][j] - m2.data[i][j])<0?0: (m1.data[i][j] - m2.data[i][j]);
	return sub;
}
void Swap(Matrix& a, Matrix& b)
{
	int tempw = a.width;
	a.width = b.width;
	b.width = tempw;
	int temph = a.height;
	a.height = b.height;
	b.height = temph;
	double** tempd;
	tempd = a.data;
	a.data = b.data;
	b.data = tempd;
}


Matrix& Matrix::operator=(const Matrix& m) //重载赋值运算符，完成对象间的深拷贝；
{
	height = m.height;
	width = m.width;
	data = new double* [height];
	for (int i = 0;i < m.height;i++)
	{
		data[i] = new double[width];
		for (int j = 0;j < m.width;j++)
			data[i][j] = m.data[i][j];
	}
	cout << "深拷贝成功" << endl;
	return *this;
}
bool Matrix::operator==(const Matrix& m)  //判断两个Matrix对象是否相等
{
	bool flag = this->height == m.height && this->width == m.width ? 1 : 0;
	if (flag)
		for (int i = 0;i < height;i++)
			for (int j = 0;j < width;j++)
				if (!(flag = this->data[i][j] == m.data[i][j] ? 1 : 0))
					break;
	return flag;
}

Matrix& Matrix::operator++()  //前置自加；
{
	for (int i = 0;i < height;i++)
		for (int j = 0;j < width;j++)
			this->data[i][j] = ++this->data[i][j] > 255 ? 255 : ++this->data[i][j];
	cout << "前置自加成功" << endl;
	return *this;

}
Matrix& Matrix::operator--()  //前置自减；
{
	for (int i = 0;i < height;i++)
		for (int j = 0;j < width;j++)
			this->data[i][j] = --this->data[i][j] < 0 ? 0 : --this->data[i][j];
	cout << "前置自减成功" << endl;
	return *this;

}
Matrix Matrix::operator++(int)  //后置自加；
{
	Matrix temp(height, width);
	for (int i = 0;i < height;i++)
		for (int j = 0;j < width;j++)
		{
			temp.data[i][j] = this->data[i][j];
			this->data[i][j] = ++this->data[i][j] > 255 ? 255 : this->data[i][j];
		}
	cout << "后置自加成功" << endl;
	return temp;


}
Matrix Matrix::operator--(int)  //后置自减；
{
	Matrix temp(height, width);
	for (int i = 0;i < height;i++)
		for (int j = 0;j < width;j++)
		{
			temp.data[i][j] = this->data[i][j];
			this->data[i][j] = --this->data[i][j] < 0 ? 0 : this->data[i][j];
		}
	cout << "后置自减成功" << endl;
	return temp;

}


Matrix operator+(const Matrix& m1, const Matrix& m2)//两个尺寸相同的矩阵，对应元素的数值相加；
{
	Matrix o(m1.height, m1.width);
	for (int i = 0;i < m1.height;i++)
		for (int j = 0;j < m1.width;j++)
			o.data[i][j] = m1.data[i][j] + m2.data[i][j] > 255 ? 255 : m1.data[i][j] + m2.data[i][j];
	cout << "两矩阵相加成功" << endl;
	return o;

}
Matrix operator+(Matrix& m, double num)//所有元素加上同一数值;
{
	for (int i = 0;i < m.height;i++)
		for (int j = 0;j < m.width;j++)
			m.data[i][j] = m.data[i][j] + num > 255 ? 255 : m.data[i][j] + num;
	cout << "矩阵加值成功（1）" << endl;
	return m;

}
Matrix operator-(const Matrix& m1, const Matrix& m2)//两个尺寸相同的矩阵，对应元素的数值相减；
{
	Matrix o(m1.height, m1.width);
	for (int i = 0;i < m1.height;i++)
		for (int j = 0;j < m1.width;j++)
			o.data[i][j] = m1.data[i][j] - m2.data[i][j] < 0 ? 0 : m1.data[i][j] - m2.data[i][j];
	cout << "两矩阵相减成功" << endl;
	return o;

}
Matrix operator-(Matrix& m, double num)//所有元素减去同一数值;
{
	for (int i = 0;i < m.height;i++)
		for (int j = 0;j < m.width;j++)
			m.data[i][j] = m.data[i][j] - num < 0 ? 0 : m.data[i][j] - num;

	cout << "矩阵减值成功（1）" << endl;
	return m;

}
Matrix operator*(const Matrix& m1, const Matrix& m2)//两幅尺寸相同的矩阵，对应元素的数值相乘；
{
	Matrix o(m1.height, m1.width);
	for (int i = 0;i < m1.height;i++)
		for (int j = 0;j < m1.width;j++)
			o.data[i][j] = m1.data[i][j] * m2.data[i][j] > 255 ? 255 : m1.data[i][j] * m2.data[i][j];
	cout << "两矩阵相乘成功" << endl;
	return o;

}
Matrix operator*(Matrix& m, double num)//所有元素乘上同一数值;
{
	for (int i = 0;i < m.height;i++)
		for (int j = 0;j < m.width;j++)
			m.data[i][j] = m.data[i][j] * num > 255 ? 255 : m.data[i][j] * num;
	cout << "矩阵乘值成功（1）" << endl;
	return m;

}
Matrix operator/(const Matrix& m1, const Matrix& m2)//两幅尺寸相同的矩阵，对应元素的数值相除；
{
	Matrix o(m1.height, m1.width);
	for (int i = 0;i < m1.height;i++)
		for (int j = 0;j < m1.width;j++)
		{
			if (m2.data[i][j] == 0)
				o.data[i][j] = 0;
			else
				o.data[i][j] = m1.data[i][j] / m2.data[i][j];
		}
	cout << "两矩阵相除成功" << endl;
	return o;

}
Matrix operator/(Matrix& m, double num)//所有元素除以同一数值;
{
	if (num == 0)
		for (int i = 0;i < m.height;i++)
			for (int j = 0;j < m.width;j++)
				m.data[i][j] = 0;
	else
		for (int i = 0;i < m.height;i++)
			for (int j = 0;j < m.width;j++)
				m.data[i][j] = m.data[i][j] / num;
	cout << "矩阵除值成功（1）" << endl;
	return m;

}
Matrix operator+(double num, Matrix& m)
{
	for (int i = 0;i < m.height;i++)
		for (int j = 0;j < m.width;j++)
			m.data[i][j] = m.data[i][j] + num > 255 ? 255 : m.data[i][j] + num;
	cout << "矩阵加值成功（2）" << endl;
	return m;

}
Matrix operator-(double num, Matrix& m)
{
	for (int i = 0;i < m.height;i++)
		for (int j = 0;j < m.width;j++)
			m.data[i][j] = m.data[i][j] - num < 0 ? 0 : m.data[i][j] - num;
	cout << "矩阵减值成功（2）" << endl;
	return m;

}
Matrix operator*(double num, Matrix& m)
{
	for (int i = 0;i < m.height;i++)
		for (int j = 0;j < m.width;j++)
			m.data[i][j] = m.data[i][j] * num > 255 ? 255 : m.data[i][j] * num;
	cout << "矩阵乘值成功（2）" << endl;
	return m;

}
Matrix operator/(double num, Matrix& m)
{
	if (num == 0)
		for (int i = 0;i < m.height;i++)
			for (int j = 0;j < m.width;j++)
				m.data[i][j] = 0;
	else
		for (int i = 0;i < m.height;i++)
			for (int j = 0;j < m.width;j++)
				m.data[i][j] = num / m.data[i][j];
	cout << "矩阵除值成功（2）" << endl;
	return m;

}
void Matrix::Normalize() // 该函数把矩阵的数据线性缩放至[0,1]区间，即把当前矩阵所有元素中的最小值min变成0，最大值max变为1，其他元素的值线性变到[0,1]区间，公式为：t’=(t-min)/max，注意除零情况、矩阵数据相同情况的处理;
{
	int rmax = 0, rmin = 0, cmax = 0, cmin = 0;
	for (int i = 0;i < height;i++)
		for (int j = 0;j < width;j++)
		{
			if (data[i][j] >= data[rmax][cmax])
			{
				rmax = i;
				cmax = j;
			}
			if (data[i][j] <= data[rmin][cmin])
			{
				rmin = i;
				cmin = j;
			}
		}
	for (int i = 0;i < height;i++)
		for (int j = 0;j < width;j++)
			data[i][j] = (data[i][j] - data[rmin][cmin]) / (data[rmax][cmax] - data[rmin][cmin]);
	cout << "矩阵线性放缩成功！" << endl;
}
void Matrix::Back()
{
	for (int i = 0;i < height;i++)
		for (int j = 0;j < width;j++)
			data[i][j] = data[i][j] * 255;
}
