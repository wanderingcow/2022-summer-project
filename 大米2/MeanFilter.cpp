#define _CRT_SECURE_NO_WARNINGS
#include "MeanFilter.h"
#include<iostream>
#include<Windows.h>

Matrix MeanFilter::Filtering(const Matrix& input)
{
	int h = input.Height();
    int	w = input.Width();
	Matrix mtemp(h, w);
	double sum,num;
	int left, right, up, down;
	for(int i=0;i<h;i++)
		for(int j=0;j<w;j++)
			mtemp.At(i,j)=input.At(i,j);
	for(int i=0;i<h;i++)
		for (int j = 0;j < w;j++)
		{
			sum = 0;
			num = 0;
			left = j - filterSize / 2;
			if (left < 0)
				left = 0;
			right = j + filterSize / 2;
			if (right >= w)
				right = w-1;
			up = i - filterSize / 2;
			if (up < 0)
				up = 0;
			down = i + filterSize / 2;
			if (down >= h)
				down = h - 1;
			int temp = left;
			for (up;up <= down;up++)
			{
				left = temp;
				for (left;left <= right;left++)
				{
					sum += input.At(up,left);
					num++;
				}
			}
			mtemp.At(i,j) = sum / num;
		}

	return mtemp;
}
