#pragma once
#ifndef FILTER_H
#define FILTER_H

#include "Image.h"

class Filter
{
public:
    Filter() {};
    Filter(int size)
    {
        filterSize = size;
    }; //���캯��
    virtual ~Filter() {}; //��������;

    virtual Matrix Filtering(const Matrix& input) = 0;  //�˲����������麯����;

protected:
    int filterSize;
};

#endif
