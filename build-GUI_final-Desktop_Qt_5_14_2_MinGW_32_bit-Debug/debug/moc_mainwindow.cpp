/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.14.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../GUIv3.0/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.14.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[65];
    char stringdata0[1516];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 5), // "Reset"
QT_MOC_LITERAL(2, 17, 0), // ""
QT_MOC_LITERAL(3, 18, 15), // "AdjustImageSize"
QT_MOC_LITERAL(4, 34, 6), // "qimage"
QT_MOC_LITERAL(5, 41, 7), // "QLabel*"
QT_MOC_LITERAL(6, 49, 6), // "qLabel"
QT_MOC_LITERAL(7, 56, 9), // "ShowImage"
QT_MOC_LITERAL(8, 66, 6), // "Image&"
QT_MOC_LITERAL(9, 73, 3), // "img"
QT_MOC_LITERAL(10, 77, 14), // "CheckImageFail"
QT_MOC_LITERAL(11, 92, 24), // "on_action_Open_triggered"
QT_MOC_LITERAL(12, 117, 9), // "SaveImage"
QT_MOC_LITERAL(13, 127, 26), // "on_pushButton_save_clicked"
QT_MOC_LITERAL(14, 154, 24), // "on_action_Save_triggered"
QT_MOC_LITERAL(15, 179, 26), // "on_pushButton_gray_clicked"
QT_MOC_LITERAL(16, 206, 29), // "on_pushButton_origin_released"
QT_MOC_LITERAL(17, 236, 28), // "on_pushButton_origin_pressed"
QT_MOC_LITERAL(18, 265, 30), // "on_pushButton_turnleft_clicked"
QT_MOC_LITERAL(19, 296, 26), // "on_pushButton_flip_clicked"
QT_MOC_LITERAL(20, 323, 29), // "on_pushButton_recover_clicked"
QT_MOC_LITERAL(21, 353, 28), // "on_pushButton_junzhi_clicked"
QT_MOC_LITERAL(22, 382, 30), // "on_pushButton_gradient_clicked"
QT_MOC_LITERAL(23, 413, 30), // "on_pushButton_yuanxing_clicked"
QT_MOC_LITERAL(24, 444, 38), // "on_horizontalSlider_erzhi_val..."
QT_MOC_LITERAL(25, 483, 5), // "value"
QT_MOC_LITERAL(26, 489, 40), // "on_horizontalSlider_erzhi_sli..."
QT_MOC_LITERAL(27, 530, 38), // "on_horizontalSlider_duibi_val..."
QT_MOC_LITERAL(28, 569, 40), // "on_horizontalSlider_duibi_sli..."
QT_MOC_LITERAL(29, 610, 34), // "on_horizontalSlider_2_valueCh..."
QT_MOC_LITERAL(30, 645, 36), // "on_horizontalSlider_2_sliderR..."
QT_MOC_LITERAL(31, 682, 30), // "on_action_meanFliter_triggered"
QT_MOC_LITERAL(32, 713, 28), // "on_action_gradient_triggered"
QT_MOC_LITERAL(33, 742, 36), // "on_action_yuanxing_clicked_tr..."
QT_MOC_LITERAL(34, 779, 26), // "on_pushButton_rice_clicked"
QT_MOC_LITERAL(35, 806, 34), // "on_horizontalSlider_R_valueCh..."
QT_MOC_LITERAL(36, 841, 36), // "on_horizontalSlider_R_sliderR..."
QT_MOC_LITERAL(37, 878, 34), // "on_horizontalSlider_G_valueCh..."
QT_MOC_LITERAL(38, 913, 36), // "on_horizontalSlider_G_sliderR..."
QT_MOC_LITERAL(39, 950, 34), // "on_horizontalSlider_B_valueCh..."
QT_MOC_LITERAL(40, 985, 36), // "on_horizontalSlider_B_sliderR..."
QT_MOC_LITERAL(41, 1022, 43), // "on_horizontalSlider_brightnes..."
QT_MOC_LITERAL(42, 1066, 38), // "on_horizontalSlider_baohe_val..."
QT_MOC_LITERAL(43, 1105, 45), // "on_horizontalSlider_brightnes..."
QT_MOC_LITERAL(44, 1151, 40), // "on_horizontalSlider_baohe_sli..."
QT_MOC_LITERAL(45, 1192, 25), // "on_action_clear_triggered"
QT_MOC_LITERAL(46, 1218, 21), // "on_action_3_triggered"
QT_MOC_LITERAL(47, 1240, 21), // "on_action_4_triggered"
QT_MOC_LITERAL(48, 1262, 29), // "on_pushButton_masaike_clicked"
QT_MOC_LITERAL(49, 1292, 6), // "Mosaic"
QT_MOC_LITERAL(50, 1299, 9), // "SetMosaic"
QT_MOC_LITERAL(51, 1309, 2), // "x1"
QT_MOC_LITERAL(52, 1312, 2), // "y1"
QT_MOC_LITERAL(53, 1315, 2), // "x2"
QT_MOC_LITERAL(54, 1318, 2), // "y2"
QT_MOC_LITERAL(55, 1321, 25), // "on_pushButton_min_clicked"
QT_MOC_LITERAL(56, 1347, 23), // "on_action_min_triggered"
QT_MOC_LITERAL(57, 1371, 11), // "eventFilter"
QT_MOC_LITERAL(58, 1383, 7), // "watched"
QT_MOC_LITERAL(59, 1391, 7), // "QEvent*"
QT_MOC_LITERAL(60, 1399, 5), // "event"
QT_MOC_LITERAL(61, 1405, 25), // "on_action_UpCat_triggered"
QT_MOC_LITERAL(62, 1431, 27), // "on_action_DownCat_triggered"
QT_MOC_LITERAL(63, 1459, 28), // "on_action_RightCat_triggered"
QT_MOC_LITERAL(64, 1488, 27) // "on_action_LeftCat_triggered"

    },
    "MainWindow\0Reset\0\0AdjustImageSize\0"
    "qimage\0QLabel*\0qLabel\0ShowImage\0Image&\0"
    "img\0CheckImageFail\0on_action_Open_triggered\0"
    "SaveImage\0on_pushButton_save_clicked\0"
    "on_action_Save_triggered\0"
    "on_pushButton_gray_clicked\0"
    "on_pushButton_origin_released\0"
    "on_pushButton_origin_pressed\0"
    "on_pushButton_turnleft_clicked\0"
    "on_pushButton_flip_clicked\0"
    "on_pushButton_recover_clicked\0"
    "on_pushButton_junzhi_clicked\0"
    "on_pushButton_gradient_clicked\0"
    "on_pushButton_yuanxing_clicked\0"
    "on_horizontalSlider_erzhi_valueChanged\0"
    "value\0on_horizontalSlider_erzhi_sliderReleased\0"
    "on_horizontalSlider_duibi_valueChanged\0"
    "on_horizontalSlider_duibi_sliderReleased\0"
    "on_horizontalSlider_2_valueChanged\0"
    "on_horizontalSlider_2_sliderReleased\0"
    "on_action_meanFliter_triggered\0"
    "on_action_gradient_triggered\0"
    "on_action_yuanxing_clicked_triggered\0"
    "on_pushButton_rice_clicked\0"
    "on_horizontalSlider_R_valueChanged\0"
    "on_horizontalSlider_R_sliderReleased\0"
    "on_horizontalSlider_G_valueChanged\0"
    "on_horizontalSlider_G_sliderReleased\0"
    "on_horizontalSlider_B_valueChanged\0"
    "on_horizontalSlider_B_sliderReleased\0"
    "on_horizontalSlider_brightness_valueChanged\0"
    "on_horizontalSlider_baohe_valueChanged\0"
    "on_horizontalSlider_brightness_sliderReleased\0"
    "on_horizontalSlider_baohe_sliderReleased\0"
    "on_action_clear_triggered\0"
    "on_action_3_triggered\0on_action_4_triggered\0"
    "on_pushButton_masaike_clicked\0Mosaic\0"
    "SetMosaic\0x1\0y1\0x2\0y2\0on_pushButton_min_clicked\0"
    "on_action_min_triggered\0eventFilter\0"
    "watched\0QEvent*\0event\0on_action_UpCat_triggered\0"
    "on_action_DownCat_triggered\0"
    "on_action_RightCat_triggered\0"
    "on_action_LeftCat_triggered"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      50,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  264,    2, 0x08 /* Private */,
       3,    2,  265,    2, 0x08 /* Private */,
       7,    1,  270,    2, 0x08 /* Private */,
      10,    0,  273,    2, 0x08 /* Private */,
      11,    0,  274,    2, 0x08 /* Private */,
      12,    0,  275,    2, 0x08 /* Private */,
      13,    0,  276,    2, 0x08 /* Private */,
      14,    0,  277,    2, 0x08 /* Private */,
      15,    0,  278,    2, 0x08 /* Private */,
      16,    0,  279,    2, 0x08 /* Private */,
      17,    0,  280,    2, 0x08 /* Private */,
      18,    0,  281,    2, 0x08 /* Private */,
      19,    0,  282,    2, 0x08 /* Private */,
      20,    0,  283,    2, 0x08 /* Private */,
      21,    0,  284,    2, 0x08 /* Private */,
      22,    0,  285,    2, 0x08 /* Private */,
      23,    0,  286,    2, 0x08 /* Private */,
      24,    1,  287,    2, 0x08 /* Private */,
      26,    0,  290,    2, 0x08 /* Private */,
      27,    1,  291,    2, 0x08 /* Private */,
      28,    0,  294,    2, 0x08 /* Private */,
      29,    1,  295,    2, 0x08 /* Private */,
      30,    0,  298,    2, 0x08 /* Private */,
      31,    0,  299,    2, 0x08 /* Private */,
      32,    0,  300,    2, 0x08 /* Private */,
      33,    0,  301,    2, 0x08 /* Private */,
      34,    0,  302,    2, 0x08 /* Private */,
      35,    1,  303,    2, 0x08 /* Private */,
      36,    0,  306,    2, 0x08 /* Private */,
      37,    1,  307,    2, 0x08 /* Private */,
      38,    0,  310,    2, 0x08 /* Private */,
      39,    1,  311,    2, 0x08 /* Private */,
      40,    0,  314,    2, 0x08 /* Private */,
      41,    1,  315,    2, 0x08 /* Private */,
      42,    1,  318,    2, 0x08 /* Private */,
      43,    0,  321,    2, 0x08 /* Private */,
      44,    0,  322,    2, 0x08 /* Private */,
      45,    0,  323,    2, 0x08 /* Private */,
      46,    0,  324,    2, 0x08 /* Private */,
      47,    0,  325,    2, 0x08 /* Private */,
      48,    0,  326,    2, 0x08 /* Private */,
      49,    0,  327,    2, 0x08 /* Private */,
      50,    4,  328,    2, 0x08 /* Private */,
      55,    0,  337,    2, 0x08 /* Private */,
      56,    0,  338,    2, 0x08 /* Private */,
      57,    2,  339,    2, 0x08 /* Private */,
      61,    0,  344,    2, 0x08 /* Private */,
      62,    0,  345,    2, 0x08 /* Private */,
      63,    0,  346,    2, 0x08 /* Private */,
      64,    0,  347,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::QImage, QMetaType::QImage, 0x80000000 | 5,    4,    6,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Bool,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   25,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   25,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   25,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   25,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   25,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   25,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   25,
    QMetaType::Void, QMetaType::Int,   25,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int,   51,   52,   53,   54,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Bool, QMetaType::QObjectStar, 0x80000000 | 59,   58,   60,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->Reset(); break;
        case 1: { QImage _r = _t->AdjustImageSize((*reinterpret_cast< QImage(*)>(_a[1])),(*reinterpret_cast< QLabel*(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< QImage*>(_a[0]) = std::move(_r); }  break;
        case 2: _t->ShowImage((*reinterpret_cast< Image(*)>(_a[1]))); break;
        case 3: { bool _r = _t->CheckImageFail();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 4: _t->on_action_Open_triggered(); break;
        case 5: _t->SaveImage(); break;
        case 6: _t->on_pushButton_save_clicked(); break;
        case 7: _t->on_action_Save_triggered(); break;
        case 8: _t->on_pushButton_gray_clicked(); break;
        case 9: _t->on_pushButton_origin_released(); break;
        case 10: _t->on_pushButton_origin_pressed(); break;
        case 11: _t->on_pushButton_turnleft_clicked(); break;
        case 12: _t->on_pushButton_flip_clicked(); break;
        case 13: _t->on_pushButton_recover_clicked(); break;
        case 14: _t->on_pushButton_junzhi_clicked(); break;
        case 15: _t->on_pushButton_gradient_clicked(); break;
        case 16: _t->on_pushButton_yuanxing_clicked(); break;
        case 17: _t->on_horizontalSlider_erzhi_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 18: _t->on_horizontalSlider_erzhi_sliderReleased(); break;
        case 19: _t->on_horizontalSlider_duibi_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 20: _t->on_horizontalSlider_duibi_sliderReleased(); break;
        case 21: _t->on_horizontalSlider_2_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 22: _t->on_horizontalSlider_2_sliderReleased(); break;
        case 23: _t->on_action_meanFliter_triggered(); break;
        case 24: _t->on_action_gradient_triggered(); break;
        case 25: _t->on_action_yuanxing_clicked_triggered(); break;
        case 26: _t->on_pushButton_rice_clicked(); break;
        case 27: _t->on_horizontalSlider_R_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 28: _t->on_horizontalSlider_R_sliderReleased(); break;
        case 29: _t->on_horizontalSlider_G_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 30: _t->on_horizontalSlider_G_sliderReleased(); break;
        case 31: _t->on_horizontalSlider_B_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 32: _t->on_horizontalSlider_B_sliderReleased(); break;
        case 33: _t->on_horizontalSlider_brightness_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 34: _t->on_horizontalSlider_baohe_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 35: _t->on_horizontalSlider_brightness_sliderReleased(); break;
        case 36: _t->on_horizontalSlider_baohe_sliderReleased(); break;
        case 37: _t->on_action_clear_triggered(); break;
        case 38: _t->on_action_3_triggered(); break;
        case 39: _t->on_action_4_triggered(); break;
        case 40: _t->on_pushButton_masaike_clicked(); break;
        case 41: _t->Mosaic(); break;
        case 42: _t->SetMosaic((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4]))); break;
        case 43: _t->on_pushButton_min_clicked(); break;
        case 44: _t->on_action_min_triggered(); break;
        case 45: { bool _r = _t->eventFilter((*reinterpret_cast< QObject*(*)>(_a[1])),(*reinterpret_cast< QEvent*(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 46: _t->on_action_UpCat_triggered(); break;
        case 47: _t->on_action_DownCat_triggered(); break;
        case 48: _t->on_action_RightCat_triggered(); break;
        case 49: _t->on_action_LeftCat_triggered(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QLabel* >(); break;
            }
            break;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MainWindow::staticMetaObject = { {
    QMetaObject::SuperData::link<QMainWindow::staticMetaObject>(),
    qt_meta_stringdata_MainWindow.data,
    qt_meta_data_MainWindow,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 50)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 50;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 50)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 50;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
