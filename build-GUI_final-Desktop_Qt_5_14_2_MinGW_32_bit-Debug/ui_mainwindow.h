/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDockWidget>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *action_Open;
    QAction *action_Save;
    QAction *action_About;
    QAction *action_meanFliter;
    QAction *action_gradient;
    QAction *action_yuanxing_clicked;
    QAction *action_3;
    QAction *action_4;
    QAction *action_clear;
    QAction *action_min;
    QAction *action_UpCat;
    QAction *action_DownCat;
    QAction *action_LeftCat;
    QAction *action_RightCat;
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QTabWidget *tabWidget;
    QWidget *tab;
    QWidget *widget;
    QLabel *label_show;
    QLabel *label_2;
    QStatusBar *statusBar;
    QDockWidget *dockWidget;
    QWidget *dockWidgetContents;
    QPushButton *pushButton_gray;
    QSlider *horizontalSlider_2;
    QLabel *label_5;
    QPushButton *pushButton_origin;
    QPushButton *pushButton_save;
    QPushButton *pushButton_turnleft;
    QPushButton *pushButton_flip;
    QPushButton *pushButton_recover;
    QMenuBar *menuBar;
    QMenu *menu_F;
    QMenu *menu_L;
    QMenu *menu_C;
    QDockWidget *dockWidget_2;
    QWidget *dockWidgetContents_5;
    QSlider *horizontalSlider_brightness;
    QLabel *label_light;
    QLabel *label_4;
    QLabel *label;
    QSlider *horizontalSlider_erzhi;
    QLabel *label_yuzhi;
    QLabel *label_9;
    QSlider *horizontalSlider_duibi;
    QLabel *label_10;
    QSlider *horizontalSlider_baohe;
    QDockWidget *dockWidget_3;
    QWidget *dockWidgetContents_6;
    QLabel *label_6;
    QSlider *horizontalSlider_R;
    QSlider *horizontalSlider_G;
    QLabel *label_7;
    QLabel *label_8;
    QSlider *horizontalSlider_B;
    QDockWidget *dockWidget_4;
    QWidget *dockWidgetContents_2;
    QPushButton *pushButton_junzhi;
    QPushButton *pushButton_gradient;
    QPushButton *pushButton_yuanxing;
    QPushButton *pushButton_rice;
    QPushButton *pushButton_masaike;
    QPushButton *pushButton_min;
    QToolBar *mainToolBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(1131, 699);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/icons/icons/codingking.png"), QSize(), QIcon::Normal, QIcon::Off);
        MainWindow->setWindowIcon(icon);
        MainWindow->setStyleSheet(QString::fromUtf8("QMainWindow{\n"
"}\n"
"\n"
"QPushButton{\n"
"border-radius:10px;\n"
"/*color: rgb(255, 255, 255);*/\n"
"background-color: rgb(255, 255, 255);\n"
"border:2px solid rgb(0,0,0);\n"
"outline: none;\n"
"}\n"
"QPushButton:pressed{\n"
"background-color: rgb(204, 204, 215);\n"
"}\n"
"QPushButton:disabled{\n"
"background-color: rgb(167, 164, 170);\n"
"}\n"
"QSlider::handle:vertical{\n"
"color:black;\n"
"}"));
        action_Open = new QAction(MainWindow);
        action_Open->setObjectName(QString::fromUtf8("action_Open"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/icons/icons/open.png"), QSize(), QIcon::Normal, QIcon::Off);
        action_Open->setIcon(icon1);
        action_Save = new QAction(MainWindow);
        action_Save->setObjectName(QString::fromUtf8("action_Save"));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/icons/icons/save.png"), QSize(), QIcon::Normal, QIcon::Off);
        action_Save->setIcon(icon2);
        action_About = new QAction(MainWindow);
        action_About->setObjectName(QString::fromUtf8("action_About"));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/icons/icons/about.png"), QSize(), QIcon::Normal, QIcon::Off);
        action_About->setIcon(icon3);
        action_meanFliter = new QAction(MainWindow);
        action_meanFliter->setObjectName(QString::fromUtf8("action_meanFliter"));
        action_gradient = new QAction(MainWindow);
        action_gradient->setObjectName(QString::fromUtf8("action_gradient"));
        action_yuanxing_clicked = new QAction(MainWindow);
        action_yuanxing_clicked->setObjectName(QString::fromUtf8("action_yuanxing_clicked"));
        action_3 = new QAction(MainWindow);
        action_3->setObjectName(QString::fromUtf8("action_3"));
        action_4 = new QAction(MainWindow);
        action_4->setObjectName(QString::fromUtf8("action_4"));
        action_clear = new QAction(MainWindow);
        action_clear->setObjectName(QString::fromUtf8("action_clear"));
        action_min = new QAction(MainWindow);
        action_min->setObjectName(QString::fromUtf8("action_min"));
        action_UpCat = new QAction(MainWindow);
        action_UpCat->setObjectName(QString::fromUtf8("action_UpCat"));
        action_DownCat = new QAction(MainWindow);
        action_DownCat->setObjectName(QString::fromUtf8("action_DownCat"));
        action_LeftCat = new QAction(MainWindow);
        action_LeftCat->setObjectName(QString::fromUtf8("action_LeftCat"));
        action_RightCat = new QAction(MainWindow);
        action_RightCat->setObjectName(QString::fromUtf8("action_RightCat"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tabWidget->setStyleSheet(QString::fromUtf8(""));
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        widget = new QWidget(tab);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setGeometry(QRect(0, 0, 761, 581));
        label_show = new QLabel(widget);
        label_show->setObjectName(QString::fromUtf8("label_show"));
        label_show->setGeometry(QRect(0, 0, 751, 571));
        label_show->setStyleSheet(QString::fromUtf8("QLabel\n"
"{\n"
"	font: 9pt \"\345\271\274\345\234\206\";\n"
"    background-color:rgb(118, 118, 118);\n"
"    color:rgb(255, 255, 255);\n"
"    font-size: 25px;\n"
"    \n"
"}"));
        label_show->setFrameShape(QFrame::WinPanel);
        label_show->setLineWidth(1);
        label_show->setAlignment(Qt::AlignCenter);
        label_2 = new QLabel(widget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(10, 540, 151, 31));
        label_2->setMinimumSize(QSize(151, 31));
        label_2->setMaximumSize(QSize(151, 31));
        tabWidget->addTab(tab, QString());

        gridLayout->addWidget(tabWidget, 0, 0, 1, 1);

        MainWindow->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);
        dockWidget = new QDockWidget(MainWindow);
        dockWidget->setObjectName(QString::fromUtf8("dockWidget"));
        dockWidget->setMinimumSize(QSize(172, 46));
        dockWidget->setFocusPolicy(Qt::StrongFocus);
        dockWidget->setStyleSheet(QString::fromUtf8(""));
        dockWidget->setFeatures(QDockWidget::NoDockWidgetFeatures);
        dockWidget->setAllowedAreas(Qt::AllDockWidgetAreas);
        dockWidgetContents = new QWidget();
        dockWidgetContents->setObjectName(QString::fromUtf8("dockWidgetContents"));
        pushButton_gray = new QPushButton(dockWidgetContents);
        pushButton_gray->setObjectName(QString::fromUtf8("pushButton_gray"));
        pushButton_gray->setGeometry(QRect(10, 20, 151, 28));
        horizontalSlider_2 = new QSlider(dockWidgetContents);
        horizontalSlider_2->setObjectName(QString::fromUtf8("horizontalSlider_2"));
        horizontalSlider_2->setGeometry(QRect(10, 160, 151, 31));
        horizontalSlider_2->setStyleSheet(QString::fromUtf8("QSlider::add-page:Horizontal\n"
"     {     \n"
"        background-color: rgb(223, 223, 223);\n"
"        height:4px;\n"
"     }\n"
"     QSlider::sub-page:Horizontal \n"
"    {\n"
"        background-color:brown;\n"
"        height:4px;\n"
"     }\n"
"    QSlider::groove:Horizontal \n"
"    {\n"
"        background:transparent;\n"
"        height:10px;\n"
"		border-radius: 5px;\n"
"    }\n"
"    QSlider::handle:Horizontal \n"
"    {\n"
"        height: 20px;\n"
"        width:20px;\n"
"        border-image: url(:/icons/icons/slide.png);\n"
"        margin: -8 0px;\n"
"    }\n"
""));
        horizontalSlider_2->setMinimum(-10);
        horizontalSlider_2->setMaximum(10);
        horizontalSlider_2->setSingleStep(1);
        horizontalSlider_2->setValue(0);
        horizontalSlider_2->setOrientation(Qt::Horizontal);
        horizontalSlider_2->setTickInterval(0);
        label_5 = new QLabel(dockWidgetContents);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(10, 140, 51, 16));
        pushButton_origin = new QPushButton(dockWidgetContents);
        pushButton_origin->setObjectName(QString::fromUtf8("pushButton_origin"));
        pushButton_origin->setGeometry(QRect(10, 60, 151, 28));
        pushButton_save = new QPushButton(dockWidgetContents);
        pushButton_save->setObjectName(QString::fromUtf8("pushButton_save"));
        pushButton_save->setGeometry(QRect(10, 100, 151, 28));
        pushButton_turnleft = new QPushButton(dockWidgetContents);
        pushButton_turnleft->setObjectName(QString::fromUtf8("pushButton_turnleft"));
        pushButton_turnleft->setGeometry(QRect(10, 200, 64, 64));
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/icons/icons/90.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_turnleft->setIcon(icon4);
        pushButton_turnleft->setIconSize(QSize(64, 64));
        pushButton_turnleft->setAutoDefault(false);
        pushButton_turnleft->setFlat(true);
        pushButton_flip = new QPushButton(dockWidgetContents);
        pushButton_flip->setObjectName(QString::fromUtf8("pushButton_flip"));
        pushButton_flip->setGeometry(QRect(90, 200, 64, 64));
        QIcon icon5;
        icon5.addFile(QString::fromUtf8(":/icons/icons/flip.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_flip->setIcon(icon5);
        pushButton_flip->setIconSize(QSize(64, 64));
        pushButton_flip->setAutoDefault(false);
        pushButton_flip->setFlat(true);
        pushButton_recover = new QPushButton(dockWidgetContents);
        pushButton_recover->setObjectName(QString::fromUtf8("pushButton_recover"));
        pushButton_recover->setGeometry(QRect(10, 280, 151, 28));
        dockWidget->setWidget(dockWidgetContents);
        MainWindow->addDockWidget(Qt::LeftDockWidgetArea, dockWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1131, 26));
        menu_F = new QMenu(menuBar);
        menu_F->setObjectName(QString::fromUtf8("menu_F"));
        menu_L = new QMenu(menuBar);
        menu_L->setObjectName(QString::fromUtf8("menu_L"));
        menu_C = new QMenu(menuBar);
        menu_C->setObjectName(QString::fromUtf8("menu_C"));
        MainWindow->setMenuBar(menuBar);
        dockWidget_2 = new QDockWidget(MainWindow);
        dockWidget_2->setObjectName(QString::fromUtf8("dockWidget_2"));
        dockWidget_2->setMinimumSize(QSize(172, 46));
        dockWidget_2->setMaximumSize(QSize(172, 524287));
        dockWidget_2->setStyleSheet(QString::fromUtf8(""));
        dockWidget_2->setFloating(false);
        dockWidget_2->setFeatures(QDockWidget::NoDockWidgetFeatures);
        dockWidget_2->setAllowedAreas(Qt::AllDockWidgetAreas);
        dockWidgetContents_5 = new QWidget();
        dockWidgetContents_5->setObjectName(QString::fromUtf8("dockWidgetContents_5"));
        horizontalSlider_brightness = new QSlider(dockWidgetContents_5);
        horizontalSlider_brightness->setObjectName(QString::fromUtf8("horizontalSlider_brightness"));
        horizontalSlider_brightness->setGeometry(QRect(10, 40, 141, 22));
        horizontalSlider_brightness->setStyleSheet(QString::fromUtf8("QSlider::add-page:Horizontal\n"
"     {     \n"
"        background-color: rgb(223, 223, 223);\n"
"        height:4px;\n"
"     }\n"
"     QSlider::sub-page:Horizontal \n"
"    {\n"
"        background-color:black;\n"
"        height:4px;\n"
"     }\n"
"    QSlider::groove:Horizontal \n"
"    {\n"
"        background:transparent;\n"
"        height:10px;\n"
"		border-radius: 5px;\n"
"    }\n"
"    QSlider::handle:Horizontal \n"
"    {\n"
"        height: 20px;\n"
"        width:20px;\n"
"        border-image: url(:/icons/icons/slide.png);\n"
"        margin: -8 0px;\n"
"    }\n"
""));
        horizontalSlider_brightness->setMinimum(-150);
        horizontalSlider_brightness->setMaximum(150);
        horizontalSlider_brightness->setOrientation(Qt::Horizontal);
        label_light = new QLabel(dockWidgetContents_5);
        label_light->setObjectName(QString::fromUtf8("label_light"));
        label_light->setGeometry(QRect(100, 10, 41, 16));
        label_4 = new QLabel(dockWidgetContents_5);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(10, 10, 81, 16));
        label = new QLabel(dockWidgetContents_5);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(10, 200, 61, 16));
        horizontalSlider_erzhi = new QSlider(dockWidgetContents_5);
        horizontalSlider_erzhi->setObjectName(QString::fromUtf8("horizontalSlider_erzhi"));
        horizontalSlider_erzhi->setGeometry(QRect(10, 230, 141, 22));
        horizontalSlider_erzhi->setStyleSheet(QString::fromUtf8("QSlider::add-page:Horizontal\n"
"     {     \n"
"        background-color: rgb(223, 223, 223);\n"
"        height:4px;\n"
"     }\n"
"     QSlider::sub-page:Horizontal \n"
"    {\n"
"        background-color:black;\n"
"        height:4px;\n"
"     }\n"
"    QSlider::groove:Horizontal \n"
"    {\n"
"        background:transparent;\n"
"        height:10px;\n"
"		border-radius: 5px;\n"
"    }\n"
"    QSlider::handle:Horizontal \n"
"    {\n"
"        height: 20px;\n"
"        width:20px;\n"
"        border-image: url(:/icons/icons/slide.png);\n"
"        margin: -8 0px;\n"
"    }\n"
""));
        horizontalSlider_erzhi->setMaximum(255);
        horizontalSlider_erzhi->setValue(128);
        horizontalSlider_erzhi->setOrientation(Qt::Horizontal);
        horizontalSlider_erzhi->setTickPosition(QSlider::NoTicks);
        label_yuzhi = new QLabel(dockWidgetContents_5);
        label_yuzhi->setObjectName(QString::fromUtf8("label_yuzhi"));
        label_yuzhi->setGeometry(QRect(80, 200, 72, 15));
        label_9 = new QLabel(dockWidgetContents_5);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setGeometry(QRect(10, 80, 72, 15));
        horizontalSlider_duibi = new QSlider(dockWidgetContents_5);
        horizontalSlider_duibi->setObjectName(QString::fromUtf8("horizontalSlider_duibi"));
        horizontalSlider_duibi->setGeometry(QRect(10, 100, 141, 22));
        horizontalSlider_duibi->setStyleSheet(QString::fromUtf8("QSlider::add-page:Horizontal\n"
"     {     \n"
"        background-color: rgb(223, 223, 223);\n"
"        height:4px;\n"
"     }\n"
"     QSlider::sub-page:Horizontal \n"
"    {\n"
"        background-color:black;\n"
"        height:4px;\n"
"     }\n"
"    QSlider::groove:Horizontal \n"
"    {\n"
"        background:transparent;\n"
"        height:10px;\n"
"		border-radius: 5px;\n"
"    }\n"
"    QSlider::handle:Horizontal \n"
"    {\n"
"        height: 20px;\n"
"        width:20px;\n"
"        border-image: url(:/icons/icons/slide.png);\n"
"        margin: -8 0px;\n"
"    }\n"
""));
        horizontalSlider_duibi->setMinimum(-255);
        horizontalSlider_duibi->setMaximum(255);
        horizontalSlider_duibi->setOrientation(Qt::Horizontal);
        label_10 = new QLabel(dockWidgetContents_5);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setGeometry(QRect(10, 140, 72, 15));
        horizontalSlider_baohe = new QSlider(dockWidgetContents_5);
        horizontalSlider_baohe->setObjectName(QString::fromUtf8("horizontalSlider_baohe"));
        horizontalSlider_baohe->setGeometry(QRect(10, 160, 141, 22));
        horizontalSlider_baohe->setFocusPolicy(Qt::StrongFocus);
        horizontalSlider_baohe->setStyleSheet(QString::fromUtf8("QSlider::add-page:Horizontal\n"
"     {     \n"
"        background-color: rgb(223, 223, 223);\n"
"        height:4px;\n"
"     }\n"
"     QSlider::sub-page:Horizontal \n"
"    {\n"
"        background-color:black;\n"
"        height:4px;\n"
"     }\n"
"    QSlider::groove:Horizontal \n"
"    {\n"
"        background:transparent;\n"
"        height:10px;\n"
"		border-radius: 5px;\n"
"    }\n"
"    QSlider::handle:Horizontal \n"
"    {\n"
"        height: 20px;\n"
"        width:20px;\n"
"        border-image: url(:/icons/icons/slide.png);\n"
"        margin: -8 0px;\n"
"    }\n"
""));
        horizontalSlider_baohe->setOrientation(Qt::Horizontal);
        dockWidget_2->setWidget(dockWidgetContents_5);
        MainWindow->addDockWidget(Qt::RightDockWidgetArea, dockWidget_2);
        dockWidget_3 = new QDockWidget(MainWindow);
        dockWidget_3->setObjectName(QString::fromUtf8("dockWidget_3"));
        dockWidget_3->setMinimumSize(QSize(172, 46));
        dockWidget_3->setMaximumSize(QSize(172, 524287));
        dockWidget_3->setStyleSheet(QString::fromUtf8(""));
        dockWidget_3->setFeatures(QDockWidget::NoDockWidgetFeatures);
        dockWidgetContents_6 = new QWidget();
        dockWidgetContents_6->setObjectName(QString::fromUtf8("dockWidgetContents_6"));
        label_6 = new QLabel(dockWidgetContents_6);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setGeometry(QRect(10, 20, 72, 15));
        horizontalSlider_R = new QSlider(dockWidgetContents_6);
        horizontalSlider_R->setObjectName(QString::fromUtf8("horizontalSlider_R"));
        horizontalSlider_R->setGeometry(QRect(9, 50, 141, 22));
        horizontalSlider_R->setContextMenuPolicy(Qt::DefaultContextMenu);
        horizontalSlider_R->setToolTipDuration(-1);
        horizontalSlider_R->setLayoutDirection(Qt::LeftToRight);
        horizontalSlider_R->setStyleSheet(QString::fromUtf8("QSlider::add-page:Horizontal\n"
"     {     \n"
"        background-color: rgb(223, 223, 223);\n"
"        height:4px;\n"
"     }\n"
"     QSlider::sub-page:Horizontal \n"
"    {\n"
"        background-color:red;\n"
"        height:4px;\n"
"     }\n"
"    QSlider::groove:Horizontal \n"
"    {\n"
"        background:transparent;\n"
"        height:10px;\n"
"		border-radius: 5px;\n"
"    }\n"
"    QSlider::handle:Horizontal \n"
"    {\n"
"        height: 20px;\n"
"        width:20px;\n"
"        border-image: url(:/icons/icons/slide.png);\n"
"        margin: -8 0px;\n"
"    }\n"
""));
        horizontalSlider_R->setMaximum(100);
        horizontalSlider_R->setOrientation(Qt::Horizontal);
        horizontalSlider_R->setTickPosition(QSlider::NoTicks);
        horizontalSlider_G = new QSlider(dockWidgetContents_6);
        horizontalSlider_G->setObjectName(QString::fromUtf8("horizontalSlider_G"));
        horizontalSlider_G->setGeometry(QRect(10, 120, 141, 22));
        horizontalSlider_G->setStyleSheet(QString::fromUtf8("QSlider::add-page:Horizontal\n"
"     {     \n"
"        background-color: rgb(223, 223, 223);\n"
"        height:4px;\n"
"     }\n"
"     QSlider::sub-page:Horizontal \n"
"    {\n"
"        background-color:green;\n"
"        height:4px;\n"
"     }\n"
"    QSlider::groove:Horizontal \n"
"    {\n"
"        background:transparent;\n"
"        height:10px;\n"
"		border-radius: 5px;\n"
"    }\n"
"    QSlider::handle:Horizontal \n"
"    {\n"
"        height: 20px;\n"
"        width:20px;\n"
"        border-image: url(:/icons/icons/slide.png);\n"
"        margin: -8 0px;\n"
"    }\n"
""));
        horizontalSlider_G->setMaximum(100);
        horizontalSlider_G->setOrientation(Qt::Horizontal);
        label_7 = new QLabel(dockWidgetContents_6);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setGeometry(QRect(11, 90, 72, 15));
        label_8 = new QLabel(dockWidgetContents_6);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setGeometry(QRect(10, 160, 72, 15));
        horizontalSlider_B = new QSlider(dockWidgetContents_6);
        horizontalSlider_B->setObjectName(QString::fromUtf8("horizontalSlider_B"));
        horizontalSlider_B->setGeometry(QRect(9, 190, 141, 22));
        horizontalSlider_B->setStyleSheet(QString::fromUtf8("QSlider::add-page:Horizontal\n"
"     {     \n"
"        background-color: rgb(223, 223, 223);\n"
"        height:4px;\n"
"     }\n"
"     QSlider::sub-page:Horizontal \n"
"    {\n"
"        background-color:blue;\n"
"        height:4px;\n"
"     }\n"
"    QSlider::groove:Horizontal \n"
"    {\n"
"        background:transparent;\n"
"        height:10px;\n"
"		border-radius: 5px;\n"
"    }\n"
"    QSlider::handle:Horizontal \n"
"    {\n"
"        height: 20px;\n"
"        width:20px;\n"
"        border-image: url(:/icons/icons/slide.png);\n"
"        margin: -8 0px;\n"
"    }\n"
""));
        horizontalSlider_B->setMaximum(100);
        horizontalSlider_B->setOrientation(Qt::Horizontal);
        dockWidget_3->setWidget(dockWidgetContents_6);
        MainWindow->addDockWidget(Qt::RightDockWidgetArea, dockWidget_3);
        dockWidget_4 = new QDockWidget(MainWindow);
        dockWidget_4->setObjectName(QString::fromUtf8("dockWidget_4"));
        dockWidget_4->setMinimumSize(QSize(172, 268));
        dockWidget_4->setMaximumSize(QSize(172, 268));
        dockWidget_4->setStyleSheet(QString::fromUtf8(""));
        dockWidget_4->setFeatures(QDockWidget::NoDockWidgetFeatures);
        dockWidgetContents_2 = new QWidget();
        dockWidgetContents_2->setObjectName(QString::fromUtf8("dockWidgetContents_2"));
        pushButton_junzhi = new QPushButton(dockWidgetContents_2);
        pushButton_junzhi->setObjectName(QString::fromUtf8("pushButton_junzhi"));
        pushButton_junzhi->setGeometry(QRect(10, 10, 151, 28));
        pushButton_gradient = new QPushButton(dockWidgetContents_2);
        pushButton_gradient->setObjectName(QString::fromUtf8("pushButton_gradient"));
        pushButton_gradient->setGeometry(QRect(10, 50, 151, 28));
        pushButton_yuanxing = new QPushButton(dockWidgetContents_2);
        pushButton_yuanxing->setObjectName(QString::fromUtf8("pushButton_yuanxing"));
        pushButton_yuanxing->setGeometry(QRect(10, 90, 151, 28));
        pushButton_rice = new QPushButton(dockWidgetContents_2);
        pushButton_rice->setObjectName(QString::fromUtf8("pushButton_rice"));
        pushButton_rice->setGeometry(QRect(10, 130, 151, 28));
        pushButton_masaike = new QPushButton(dockWidgetContents_2);
        pushButton_masaike->setObjectName(QString::fromUtf8("pushButton_masaike"));
        pushButton_masaike->setGeometry(QRect(10, 170, 151, 28));
        pushButton_min = new QPushButton(dockWidgetContents_2);
        pushButton_min->setObjectName(QString::fromUtf8("pushButton_min"));
        pushButton_min->setGeometry(QRect(10, 210, 151, 28));
        dockWidget_4->setWidget(dockWidgetContents_2);
        MainWindow->addDockWidget(Qt::LeftDockWidgetArea, dockWidget_4);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);

        menuBar->addAction(menu_F->menuAction());
        menuBar->addAction(menu_L->menuAction());
        menuBar->addAction(menu_C->menuAction());
        menu_F->addAction(action_Open);
        menu_F->addAction(action_Save);
        menu_F->addSeparator();
        menu_F->addAction(action_clear);
        menu_L->addAction(action_meanFliter);
        menu_L->addAction(action_gradient);
        menu_L->addAction(action_yuanxing_clicked);
        menu_L->addAction(action_3);
        menu_L->addAction(action_4);
        menu_L->addAction(action_min);
        menu_C->addAction(action_UpCat);
        menu_C->addAction(action_DownCat);
        menu_C->addAction(action_LeftCat);
        menu_C->addAction(action_RightCat);
        mainToolBar->addAction(action_Open);
        mainToolBar->addAction(action_Save);
        mainToolBar->addSeparator();

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(0);
        pushButton_turnleft->setDefault(false);
        pushButton_flip->setDefault(false);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "\345\233\276\345\203\217\345\244\204\347\220\206", nullptr));
        action_Open->setText(QCoreApplication::translate("MainWindow", "\346\211\223\345\274\200(&O)", nullptr));
#if QT_CONFIG(tooltip)
        action_Open->setToolTip(QCoreApplication::translate("MainWindow", "\346\211\223\345\274\200", nullptr));
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(shortcut)
        action_Open->setShortcut(QCoreApplication::translate("MainWindow", "Ctrl+O", nullptr));
#endif // QT_CONFIG(shortcut)
        action_Save->setText(QCoreApplication::translate("MainWindow", "\344\277\235\345\255\230(&S)", nullptr));
#if QT_CONFIG(tooltip)
        action_Save->setToolTip(QCoreApplication::translate("MainWindow", "\344\277\235\345\255\230", nullptr));
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(shortcut)
        action_Save->setShortcut(QCoreApplication::translate("MainWindow", "Ctrl+S", nullptr));
#endif // QT_CONFIG(shortcut)
        action_About->setText(QCoreApplication::translate("MainWindow", "\345\205\263\344\272\216(&A)", nullptr));
#if QT_CONFIG(tooltip)
        action_About->setToolTip(QCoreApplication::translate("MainWindow", "\345\205\263\344\272\216", nullptr));
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(shortcut)
        action_About->setShortcut(QCoreApplication::translate("MainWindow", "Ctrl+A", nullptr));
#endif // QT_CONFIG(shortcut)
        action_meanFliter->setText(QCoreApplication::translate("MainWindow", "\345\235\207\345\200\274\346\273\244\346\263\242", nullptr));
        action_gradient->setText(QCoreApplication::translate("MainWindow", "\350\276\271\347\274\230\346\243\200\346\265\213", nullptr));
        action_yuanxing_clicked->setText(QCoreApplication::translate("MainWindow", "\345\234\206\345\275\242\346\243\200\346\265\213", nullptr));
        action_3->setText(QCoreApplication::translate("MainWindow", "\345\244\247\347\261\263\346\243\200\346\265\213", nullptr));
        action_4->setText(QCoreApplication::translate("MainWindow", "\351\251\254\350\265\233\345\205\213", nullptr));
        action_clear->setText(QCoreApplication::translate("MainWindow", "\346\270\205\347\251\272", nullptr));
        action_min->setText(QCoreApplication::translate("MainWindow", "\350\264\237\347\211\207", nullptr));
        action_UpCat->setText(QCoreApplication::translate("MainWindow", "\344\270\212\346\216\245", nullptr));
        action_DownCat->setText(QCoreApplication::translate("MainWindow", "\344\270\213\346\216\245", nullptr));
        action_LeftCat->setText(QCoreApplication::translate("MainWindow", "\345\267\246\346\216\245", nullptr));
        action_RightCat->setText(QCoreApplication::translate("MainWindow", "\345\217\263\346\216\245", nullptr));
        label_show->setText(QCoreApplication::translate("MainWindow", "\346\213\226\346\213\275\346\226\207\344\273\266(.bmp)\350\207\263\346\255\244\345\244\204", nullptr));
        label_2->setText(QString());
        tabWidget->setTabText(tabWidget->indexOf(tab), QCoreApplication::translate("MainWindow", "\345\233\276\345\203\217\345\244\204\347\220\206", nullptr));
        dockWidget->setWindowTitle(QCoreApplication::translate("MainWindow", "\346\230\276\347\244\272", nullptr));
        pushButton_gray->setText(QCoreApplication::translate("MainWindow", "\347\201\260\345\272\246\345\214\226", nullptr));
        label_5->setText(QCoreApplication::translate("MainWindow", "\346\257\224\344\276\213\357\274\232", nullptr));
        pushButton_origin->setText(QCoreApplication::translate("MainWindow", "\346\230\276\347\244\272\345\216\237\345\233\276", nullptr));
        pushButton_save->setText(QCoreApplication::translate("MainWindow", "\344\277\235\345\255\230\345\233\276\347\211\207", nullptr));
        pushButton_turnleft->setText(QString());
        pushButton_flip->setText(QString());
        pushButton_recover->setText(QCoreApplication::translate("MainWindow", "\346\201\242\345\244\215\345\216\237\345\233\276", nullptr));
        menu_F->setTitle(QCoreApplication::translate("MainWindow", "\346\226\207\344\273\266(&F)", nullptr));
        menu_L->setTitle(QCoreApplication::translate("MainWindow", "\346\225\210\346\236\234(&L)", nullptr));
        menu_C->setTitle(QCoreApplication::translate("MainWindow", "\346\213\274\345\233\276(&C)", nullptr));
        dockWidget_2->setWindowTitle(QCoreApplication::translate("MainWindow", "\346\273\244\351\225\234", nullptr));
        label_light->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        label_4->setText(QCoreApplication::translate("MainWindow", "\344\272\256\345\272\246\357\274\232", nullptr));
        label->setText(QCoreApplication::translate("MainWindow", "\344\272\214\345\200\274\345\214\226\357\274\232", nullptr));
        label_yuzhi->setText(QCoreApplication::translate("MainWindow", "128", nullptr));
        label_9->setText(QCoreApplication::translate("MainWindow", "\345\257\271\346\257\224\345\272\246\357\274\232", nullptr));
        label_10->setText(QCoreApplication::translate("MainWindow", "\351\245\261\345\222\214\345\272\246\357\274\232", nullptr));
        dockWidget_3->setWindowTitle(QCoreApplication::translate("MainWindow", "\350\211\262\345\275\251\345\242\236\345\274\272", nullptr));
        label_6->setText(QCoreApplication::translate("MainWindow", "R\345\200\274\345\242\236\345\274\272\357\274\232", nullptr));
        label_7->setText(QCoreApplication::translate("MainWindow", "G\345\200\274\345\242\236\345\274\272\357\274\232", nullptr));
        label_8->setText(QCoreApplication::translate("MainWindow", "B\345\200\274\345\242\236\345\274\272\357\274\232", nullptr));
        dockWidget_4->setWindowTitle(QCoreApplication::translate("MainWindow", "\345\267\245\345\205\267", nullptr));
        pushButton_junzhi->setText(QCoreApplication::translate("MainWindow", "\345\235\207\345\200\274\346\273\244\346\263\242", nullptr));
        pushButton_gradient->setText(QCoreApplication::translate("MainWindow", "\350\276\271\347\274\230\346\243\200\346\265\213", nullptr));
        pushButton_yuanxing->setText(QCoreApplication::translate("MainWindow", "\345\234\206\345\275\242\346\243\200\346\265\213", nullptr));
        pushButton_rice->setText(QCoreApplication::translate("MainWindow", "\345\244\247\347\261\263\346\243\200\346\265\213", nullptr));
        pushButton_masaike->setText(QCoreApplication::translate("MainWindow", "\351\251\254\350\265\233\345\205\213", nullptr));
        pushButton_min->setText(QCoreApplication::translate("MainWindow", "\350\264\237\347\211\207", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
