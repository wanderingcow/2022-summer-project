#define _CRT_SECURE_NO_WARNINGS
#include "Image.h"
#include<iostream>
#include<Windows.h>

using namespace std;

BITMAPFILEHEADER filehead;
BITMAPINFOHEADER infohead;
//构造函数
Image::Image():Matrix()
{
	cout << "调用默认构造函数成功" << endl;
}

//构造函数重载
Image::Image(int h, int w):Matrix(h, w)
{
	cout << "调用构造函数成功" << endl;
}
Image::Image(int h, int w, unsigned char val):Matrix(h, w, val)
{
	cout << "调用构造函数成功" << endl;
}
Image::Image(const char* ImageName)
{

	FILE* fp = fopen(ImageName, "rb");
	if (!fp)
	{
		cout << "Error!" << endl;
		exit(0);
	}
	fread(&filehead, sizeof(BITMAPFILEHEADER), 1, fp);
	fread(&infohead, sizeof(BITMAPINFOHEADER), 1, fp);
	width = infohead.biWidth;
	height = infohead.biHeight;
	unsigned char** temp = new unsigned char* [height];
	for (int i = 0;i < height;i++)
		temp[i] = new unsigned char[width*3];
	data = new double* [height];
	for (int i = 0;i < height;i++)
		data[i] = new double[width*3];
	for (int i = 0;i < height;i++)
		for (int j = 0;j < width*3;j++)
			fread(&temp[i][j], 1, 1, fp);

	//宽度可被4整除
	width = (width * infohead.biBitCount / 8 + 3) / 4 * 4;
	width = width / 3;

	//图片灰化
	for (int i = 0;i < height;i++)
		for (int j = 0;j < width*3;j += 3)
		{
			temp[i][j] = (temp[i][j] + temp[i][j + 1] + temp[i][j + 2]) / 3;
			temp[i][j + 1] = temp[i][j];
			temp[i][j + 2] = temp[i][j];
		}

	cout << "图片灰化成功！" << endl;
	for (int i = 0;i < height;i++)
		for (int j = 0;j < width * 3;j++)
			data[i][j] = (double)temp[i][j];
	width *= 3;
	for (int i = 0;i < height;i++)
		delete[]temp[i];
	delete[]temp;
	cout << "调用构造函数成功" << endl;
}

Image::Image(unsigned char m[][100], int rows)
{
	height = rows;
	width = 100 ;
	data = new double* [height];
	for (int i = 0;i < height;i++)
		data[i] = new double[width];
	for (int i = 0;i < height;i++)
		for (int j = 0;j < width;j++)
			data[i][j] = (double)m[i][j];
	cout << "调用构造函数成功" << endl;
}
Image::Image(unsigned char** m, int h, int w)
{
	height = h;
	width = w ;
	data = new double* [height];
	for (int i = 0;i < height;i++)
		data[i] = new double[width];
	for (int i = 0;i < height;i++)
		for (int j = 0;j < width;j++)
			data[i][j] = (double)m[i][j];
	cout << "调用构造函数成功" << endl;
}
Image::Image(const Matrix& m):Matrix(m)
{

	cout << "调用构造函数成功" << endl;

}
Image::Image(const Image& im)
{
	height = im.height;
	width = im.width ;
	data = new double* [height];
	for (int i = 0;i < height;i++)
		data[i] = new double[width];
	for (int i = 0;i < height;i++)
		for (int j = 0;j < width;j++)
			data[i][j] = im.data[i][j];
	cout << "调用构造函数成功" << endl;
}

Image::~Image()
{
	/*for (int i = 0;i < height;i++)
		delete[]data[i];
	delete[]data;*/

}

//读取BMP文件的图像
void Image::ReadBMP(const char* filename)
{
	FILE* fp = fopen(filename, "rb");
	if (!fp)
	{
		cout << "Error!" << endl;
		exit(0);
	}
	fread(&filehead, sizeof(BITMAPFILEHEADER), 1, fp);
	fread(&infohead, sizeof(BITMAPINFOHEADER), 1, fp);
	width = infohead.biWidth;
	height = infohead.biHeight;
	unsigned char** temp = new unsigned char* [height];
	for (int i = 0;i < height;i++)
		temp[i] = new unsigned char[width * 3];
	for (int i = 0;i < height;i++)
		for (int j = 0;j < width * 3;j++)
			fread(&temp[i][j], 1, 1, fp);
	cout << "读入成功" << endl;
	//宽度可被4整除
	width = (width * infohead.biBitCount / 8 + 3) / 4 * 4;
	width = width / 3;
	//图片灰化
	for (int i = 0;i < height;i++)
		for (int j = 0;j < width * 3;j += 3)
		{
			temp[i][j] = (temp[i][j] + temp[i][j + 1] + temp[i][j + 2]) / 3;
			temp[i][j + 1] = temp[i][j];
			temp[i][j + 2] = temp[i][j];
		}
	data = new double* [height];
	for (int i = 0;i < height;i++)
		data[i] = new double[width * 3];
	for (int i = 0;i < height;i++)
		for (int j = 0;j < width * 3;j++)
			data[i][j] = (double)temp[i][j];
	width *= 3;
	for (int i = 0;i < height;i++)
		delete[]temp[i];
	delete[]temp;
	cout << "图片灰化成功！" << endl;
	fclose(fp);
}
//将图像写入BMP文件
void Image::WriteBMP(const char* filename)
{
	FILE* fp = fopen("Fruits.bmp", "rb");
	if (!fp)
	{
		cout << "Error!" << endl;
		exit(0);
	}
	fread(&filehead, sizeof(BITMAPFILEHEADER), 1, fp);
	fread(&infohead, sizeof(BITMAPINFOHEADER), 1, fp);
	fclose(fp);
	infohead.biHeight = height;
	infohead.biWidth = width /3;
	//infohead.biBitCount = infohead.biBitCount == 0 ? 24 : infohead.biBitCount;
	infohead.biWidth = (infohead.biWidth * infohead.biBitCount / 8 + 3) / 4 * 4;
	infohead.biWidth = infohead.biWidth / 3;
	infohead.biSizeImage = height * width;
	FILE* wfp = fopen(filename, "wb");
	fwrite(&filehead, sizeof(BITMAPFILEHEADER), 1, wfp);
	fwrite(&infohead, sizeof(BITMAPINFOHEADER), 1, wfp);
	unsigned char** temp = new unsigned char* [height];
	for (int i = 0;i < height;i++)
		temp[i] = new unsigned char[width];
	for (int i = 0;i < height;i++)
		for (int j = 0;j < width;j++)
            temp[i][j] = (unsigned char)data[i][j];
	for (int i = 0;i < height;i++)
		fwrite(temp[i], 1, width, wfp);
	fclose(wfp);
	for (int i = 0;i < height;i++)
		delete[]temp[i];
	delete[]temp;
	cout << "图片写入成功！" << endl;
}

//翻转
void Image::Flip(int code)
{
	if (code)
	{
		int i = height - 1, j = 0;         //双指针
		while (j < i)
		{
			double* temp = data[i];
			data[i] = data[j];
			data[j] = temp;
			i--;
			j++;
		}
		cout << "上下翻转成功！" << endl;
	}
	else
	{
		for (int i = 0;i < height;i++)
		{
			int j = 0, k = width - 1;               //双指针
			while (j < k)
			{
				double temp = data[i][j];
				data[i][j] = data[i][k];
				data[i][k] = temp;
				j++;
				k--;
			}
		}
		cout << "左右翻转成功！" << endl;
	}
}
//放缩
void Image::Resize(int h, int w)
{
	int ht = h / height;
	int wt = w*3 / width;
	double** temp = new double* [h];
	for (int i = 0;i < h;i++)
		temp[i] = new double[w*3];
	if (ht < 1)
	{
		for (int i = 0,m=0;i < h;i++,m+=height/h)
		{
			if (wt < 1)
				for (int j = 0, n = 0;j < w*3;j++, n += width/(w*3))
					temp[i][j] = data[m][n];
			else
				for (int j = 0;j < width;j++)
					for (int x = 0;x < wt;x++)
						temp[i][j*wt+ x] = data[m][j];
		}
	}
	else
	{
		for (int i = 0;i < height;i++)
			for(int x=0;x<ht;x++)
		    {
			    if (wt < 1)
			    	for (int j = 0, n = 0;j < w*3;j++, n += width / (w * 3))
			    		temp[i*ht+x][j] = data[i][n];
			    else
				    for (int j = 0;j < w*3;j++)
				    	for (int y = 0;y < wt;x++)
				    		temp[i*ht+x][j*wt + y] = data[i][j];
		    }
	}
	for (int i = 0;i < height;i++)
		delete[]data[i];
	delete[]data;
	height = h;
	width = w*3;
	data = new double* [height];
	for (int i = 0;i < height;i++)
		data[i] = new double[width];
	for (int i = 0;i < height;i++)
		for (int j = 0;j < width;j++)
			data[i][j] = temp[i][j];
	for (int i = 0;i < height;i++)
		delete[]temp[i];
	delete[]temp;
	cout << "放缩成功！" << endl;

}
//裁剪
void Image::Cut(int x1, int y1, int x2, int y2)
{
	long h = height;
	long w = width;
	//计算裁剪后的尺寸
	width /= 3;
	height = y2 < y1 ? y1 - y2 : y2 - y1;
	width = x2 < x1 ? x1 - x2 : x2 - x1;
	width *= 3;
	//计算起始点
	int x = x1 < x2 ? x1 : x2;
	int y = y1 < y2 ? y1 : y2;
	double** crop = new double* [height];
	for (int i = 0;i < height;i++)
		crop[i] = new double[width];
	for (int m = 0;m < height;m++)
		for (int n = 0;n < width;n++)
			crop[m][n] = data[m + y][n + x];
	for (int i = 0;i < h;i++)
		delete[]data[i];
	delete[]data;
	data = new double* [height];
	for (int i = 0;i < height;i++)
		data[i] = new double[width];
	for (int i = 0;i < height;i++)
		for (int j = 0;j < width;j++)
			data[i][j] = crop[i][j];
	for (int i = 0;i < height;i++)
		delete[]crop[i];
	delete[] crop;

	cout << "裁剪成功！" << endl;

}
//旋转
void Image::Rotate(int degree)
{

	for (int n = 0;n < fabs(degree/90);n++)
	{
		double** rotate = new double* [width / 3];
		for (int i = 0;i < width / 3;i++)
			rotate[i] = new double[height * 3];
		for (int i = 0, k = width - 1; i < width / 3; i++, k -= 3)
			for (int j = 0;j < height;j++)
			{
				rotate[i][j * 3] = data[j][k];
				rotate[i][j * 3 + 1] = data[j][k];
				rotate[i][j * 3 + 2] = data[j][k];
			}
		for (int i = 0;i < height;i++)
			delete[]data[i];
		delete[]data;
		int temp = width / 3;
		width = height * 3;
		height = temp;
		data = new double* [height];
		for (int i = 0;i < height;i++)
			data[i] = new double[width];
		for (int i = 0;i < height;i++)
			for (int j = 0;j < width;j++)
				data[i][j] = rotate[i][j];
		for (int i = 0;i < height;i++)
			delete[]rotate[i];
		delete[] rotate;
	}
	cout << "旋转成功！" << endl;

}
//求平均值
double Image::Mean()
{
	double m=0;
	for (int i = 0;i < height;i++)
		for (int j = 0;j < width;j++)
			m += data[i][j];
	m /= height * width;
	return m;
}

double Image::Variance()
{
	double m=0,var=0;
	for (int i = 0;i < height;i++)
		for (int j = 0;j < width;j++)
			m += data[i][j];
	m /= height * width;
	for (int i = 0;i < height;i++)
		for (int j = 0;j < width;j++)
			var += (data[i][j] - m) * (data[i][j] - m);
	var /= height * width;
	return var;
}

Image Image::operator-()//对象图像取反，把所有像素的值都规整到[0,1]之间，然后每个像素都被1.0减
{
	Image temp(height, width);
	for (int i = 0;i < height;i++)
		for (int j = 0;j < width;j++)
			temp.data[i][j] = 1 - data[i][j];
	cout << "图像取反成功" << endl;
	return temp;

}
void Image::gray2bw(double t)//以给定阈值t对图像进行二值化，返回图像对象
{
	for (int i = 0;i < height;i++)
		for (int j = 0;j < width;j++)
			data[i][j] = data[i][j] < t ? 0 : 1;
	cout << "图像二值化成功" << endl;
	//return *this;
}
