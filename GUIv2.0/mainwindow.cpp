#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <iostream>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::WindowCloseButtonHint|Qt::WindowMinimizeButtonHint);
    this->setFixedSize(this->width(),this->height());
}

MainWindow::~MainWindow()
{
    delete ui;
}

QImage MainWindow::AdjustImageSize(QImage qimage,QLabel *qLabel)
{
    QImage image;
    QSize imageSize = qimage.size();
    QSize labelSize = qLabel->size();

    double widthRatio = 1.0*imageSize.width() / labelSize.width();
    double heightRatio = 1.0*imageSize.height() / labelSize.height();
    if (widthRatio>heightRatio)
        image = qimage.scaledToWidth(labelSize.width());
    else
        image = qimage.scaledToHeight(labelSize.height());
    return image;
}

void MainWindow::ShowImage(Image& img)
{
    QImage image(img.GetData(),img.GetWidth(),img.GetHeight(),QImage::Format_BGR888);
    // image = AdjustImageSize(image,ui->label_show);
    ui->label_show->setPixmap(QPixmap::fromImage(image));
    ui->label_show->setAlignment((Qt::AlignCenter));
}

void MainWindow::Reset()
{
    ui->horizontalSlider_duibi->setValue(0);
    ui->horizontalSlider_erzhi->setValue(128);
}

void MainWindow::on_action_Open_triggered()
{
    QString curDir = QDir::currentPath();
    QString fileName = QFileDialog::getOpenFileName(this, "选择图片", curDir, "Images (*.bmp)");
    if (fileName.isEmpty())
        return;

    filePath = fileName;
    srcImage.ReadBMP(filePath.toStdString().c_str());
    srcImage.Flip(1);
    curImage.ReadBMP(filePath.toStdString().c_str());
    curImage.Flip(1);

    Reset();

    std::cout << filePath.toStdString() << std::endl;

    ShowImage(curImage);
}

void MainWindow::SaveImage()
{
    if(CheckImageFail())
        return;

    QString curDir = QDir::currentPath();
    QString fileName = QFileDialog::getSaveFileName(this, "保存图片", curDir, "Images (*.bmp)");
    if (fileName.isEmpty())
        return;

    QString savePath = fileName;
    curImage.Flip(1);
    curImage.WriteBMP(savePath.toStdString().c_str());
}

void MainWindow::on_pushButton_save_clicked()
{
    SaveImage();
}


void MainWindow::on_action_Save_triggered()
{
    SaveImage();
}

bool MainWindow::CheckImageFail()
{
    if(curImage.GetData()==NULL)
    {
        QMessageBox::warning(nullptr, "提示", "请先选择一张图片！", QMessageBox::Yes |  QMessageBox::Yes);
        return true;
    }
    return false;
}

void MainWindow::on_pushButton_gray_clicked()
{
    if(CheckImageFail())
        return;

    curImage.Grayed();

    ShowImage(curImage);
}

void MainWindow::on_pushButton_origin_pressed()
{
    ShowImage(srcImage);
}

void MainWindow::on_pushButton_origin_released()
{
    ShowImage(curImage);
}

void MainWindow::on_pushButton_turnleft_clicked()
{
    if(CheckImageFail())
        return;

    curImage.Rotate(-90);

    ShowImage(curImage);
}


void MainWindow::on_pushButton_flip_clicked()
{
    if(CheckImageFail())
        return;

    curImage.Flip(0);

    ShowImage(curImage);
}


void MainWindow::on_pushButton_recover_clicked()
{
    ui->label_show->clear();
    srcImage.Reset();
    curImage.Reset();
}


void MainWindow::on_pushButton_junzhi_clicked()
{
    if(CheckImageFail())
        return;

    curImage = curImage.MeanFilter(9);

    ShowImage(curImage);
}


void MainWindow::on_pushButton_gradient_clicked()
{
    if(CheckImageFail())
        return;

    curImage = curImage.gradient();

    ShowImage(curImage);
}


void MainWindow::on_pushButton_yuanxing_clicked()
{
    if(CheckImageFail())
        return;

    Image img = curImage;

    ShowImage(img);
}


void MainWindow::on_horizontalSlider_erzhi_valueChanged(int value)
{
    if(curImage.GetData()==NULL)
    {
        ui->horizontalSlider_erzhi->setValue(128);
        return;
    }

    QString sValue = QString::number(value);
    ui->label_yuzhi->setText(QString(sValue));
    Image img = curImage;
    img.gray2bw(value);

    ShowImage(img);
}


void MainWindow::on_horizontalSlider_erzhi_sliderReleased()
{
    if(curImage.GetData()==NULL)
    {
        return;
    }

    int value = ui->horizontalSlider_erzhi->value();
    curImage.gray2bw(value);
}


void MainWindow::on_horizontalSlider_duibi_valueChanged(int value)
{
    if(curImage.GetData()==NULL)
    {
        ui->horizontalSlider_duibi->setValue(0);
        return;
    }

    double mean = curImage.Mean();

    Image img = curImage;
    img = img.Contrast(value, mean);

    ShowImage(img);
}


void MainWindow::on_horizontalSlider_duibi_sliderReleased()
{
    if(curImage.GetData()==NULL)
    {
        return;
    }

    double mean = curImage.Mean();
    int value = ui->horizontalSlider_duibi->value();
    curImage = curImage.Contrast(value,mean);
}


void MainWindow::on_horizontalSlider_2_valueChanged(int value)
{
    if(curImage.GetData()==NULL)
    {
        ui->horizontalSlider_2->setValue(0);
        return;
    }

    Image img = curImage;

    int width = img.GetWidth();
    int height = img.GetHeight();

    int newWidth = width + value * 100;
    int newHeight = height + value * 100;

    if(newWidth > ui->label_show->width() || newHeight > ui->label_show->height())
    {
        ui->horizontalSlider_2->setValue(lastSlideVal);
        return;
    }

    if(newWidth < 100 || newHeight < 100)
    {
        ui->horizontalSlider_2->setValue(lastSlideVal);
        return;
    }

    img.Resize(newHeight,newWidth);

    ShowImage(img);
    lastSlideVal = value;
}


void MainWindow::on_horizontalSlider_2_sliderReleased()
{
    if(curImage.GetData()==NULL)
    {
        return;
    }

    int value = ui->horizontalSlider_2->value();

    int width = curImage.GetWidth();
    int height = curImage.GetHeight();

    int newWidth = width + value * 100;
    int newHeight = height + value * 100;

    curImage.Resize(newHeight, newWidth);
}


void MainWindow::on_action_meanFliter_triggered()
{
    if(CheckImageFail())
        return;

    Image img = curImage.MeanFilter(9);

    ShowImage(img);
}


void MainWindow::on_action_gradient_triggered()
{
    if(CheckImageFail())
        return;

    Image img = curImage.gradient();

    ShowImage(img);
}


void MainWindow::on_pushButton_rice_clicked()
{
    if(CheckImageFail())
        return;

    curImage.Rice();

    ShowImage(curImage);
}
