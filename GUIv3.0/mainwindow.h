#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "imageclass2.0/Image.h"
#include <QLabel>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class ms_window;
class ms_window2;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void Reset();

    QImage AdjustImageSize(QImage qimage,QLabel *qLabel);

    void ShowImage(Image &img);

    bool CheckImageFail();

    void on_action_Open_triggered();

    void SaveImage();

    void on_pushButton_save_clicked();

    void on_action_Save_triggered();

    void on_pushButton_gray_clicked();

    void on_pushButton_origin_released();

    void on_pushButton_origin_pressed();

    void on_pushButton_turnleft_clicked();

    void on_pushButton_flip_clicked();

    void on_pushButton_recover_clicked();

    void on_pushButton_junzhi_clicked();

    void on_pushButton_gradient_clicked();

    void on_pushButton_yuanxing_clicked();

    void on_horizontalSlider_erzhi_valueChanged(int value);

    void on_horizontalSlider_erzhi_sliderReleased();

    void on_horizontalSlider_duibi_valueChanged(int value);

    void on_horizontalSlider_duibi_sliderReleased();

    void on_horizontalSlider_2_valueChanged(int value);

    void on_horizontalSlider_2_sliderReleased();

    void on_action_meanFliter_triggered();

    void on_action_gradient_triggered();

    void on_action_yuanxing_clicked_triggered();

    void on_pushButton_rice_clicked();

    void on_horizontalSlider_R_valueChanged(int value);

    void on_horizontalSlider_R_sliderReleased();

    void on_horizontalSlider_G_valueChanged(int value);

    void on_horizontalSlider_G_sliderReleased();

    void on_horizontalSlider_B_valueChanged(int value);

    void on_horizontalSlider_B_sliderReleased();

    void on_horizontalSlider_brightness_valueChanged(int value);

  //  void on_horizontalSlider_valueChanged(int value);

  //  void on_horizontalSlider_baohe_valueChanged(int value);

    void on_horizontalSlider_baohe_valueChanged(int value);

    void on_horizontalSlider_brightness_sliderReleased();

    void on_horizontalSlider_baohe_sliderReleased();

    void on_action_clear_triggered();

    void on_action_3_triggered();

    void on_action_4_triggered();

    void on_pushButton_masaike_clicked();

    void Mosaic();

    void SetMosaic(int x1,int y1,int x2,int y2);

    void on_pushButton_min_clicked();

    void on_action_min_triggered();

    bool eventFilter(QObject *watched, QEvent *event);

    void on_action_UpCat_triggered();

    void on_action_DownCat_triggered();

    void on_action_RightCat_triggered();

    void on_action_LeftCat_triggered();

    void on_action_Cut_triggered();

    void SetCut(int x1,int y1,int x2,int y2);

private:
    Ui::MainWindow *ui;
    ms_window *msWindow;
    ms_window2 *msWindow2;


    Image srcImage;
    Image curImage;
    QString filePath;
    int lastSlideVal=0;

    int lastR = 0;
    int lastG = 0;
    int lastB = 0;
};
#endif // MAINWINDOW_H
