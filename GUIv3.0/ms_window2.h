#ifndef MS_WINDOW2_H
#define MS_WINDOW2_H

#include <QWidget>

namespace Ui {
class ms_window2;
}

class ms_window2 : public QWidget
{
    Q_OBJECT

public:
    explicit ms_window2(QWidget *parent = nullptr);
    ~ms_window2();

private slots:
    void on_pushButton2_no_clicked();

    void on_pushButton2_yes_clicked();

private:
    Ui::ms_window2 *ui;

public:
signals:
    void SendSignal(int x1,int y1,int x2,int y2);
};

#endif // MS_WINDOW2_H
