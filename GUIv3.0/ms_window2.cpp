#include "ms_window2.h"
#include "ui_ms_window2.h"
#include <QIntValidator>
#include <QMessageBox>
#include <iostream>

ms_window2::ms_window2(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ms_window2)
{
    ui->setupUi(this);
    ui->lineEdit2_x1->setValidator(new QIntValidator(1,1000,this));
    ui->lineEdit2_y1->setValidator(new QIntValidator(1,1000,this));
    ui->lineEdit2_x2->setValidator(new QIntValidator(1,1000,this));
    ui->lineEdit2_y2->setValidator(new QIntValidator(1,1000,this));

    this->setWindowFlags(Qt::WindowCloseButtonHint|Qt::WindowMinimizeButtonHint);
    this->setFixedSize(this->width(),this->height());
}

ms_window2::~ms_window2()
{
    delete ui;
}

void ms_window2::on_pushButton2_no_clicked()
{
    close();
}


void ms_window2::on_pushButton2_yes_clicked()
{
    if(ui->lineEdit2_x1->text().isEmpty() || ui->lineEdit2_y1->text().isEmpty() || ui->lineEdit2_x2->text().isEmpty() || ui->lineEdit2_y2->text().isEmpty())
    {
        QMessageBox::warning(nullptr, "提示", "输入有误！", QMessageBox::Yes |  QMessageBox::Yes);
        return;
    }

    int x1 = ui->lineEdit2_x1->text().toInt();
    int y1 = ui->lineEdit2_y1->text().toInt();
    int x2 = ui->lineEdit2_x2->text().toInt();
    int y2 = ui->lineEdit2_y2->text().toInt();

    emit SendSignal(x1,y1,x2,y2);
    close();
}
