QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    imageclass2.0/Image.cpp \
    main.cpp \
    mainwindow.cpp \
    ms_window.cpp \
    ms_window2.cpp

HEADERS += \
    imageclass2.0/Image.h \
    mainwindow.h \
    ms_window.h \
    ms_window2.h

FORMS += \
    mainwindow.ui \
    ms_window.ui \
    ms_window2.ui

TRANSLATIONS += \
    GUI_final_zh_CN.ts
CONFIG += lrelease
CONFIG += embed_translations

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    icons.qrc


