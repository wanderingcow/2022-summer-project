#include "ms_window.h"
#include "ui_ms_window.h"
#include <QIntValidator>
#include <QMessageBox>
#include <iostream>

ms_window::ms_window(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ms_window)
{
    ui->setupUi(this);
    ui->lineEdit_x1->setValidator(new QIntValidator(1,1000,this));
    ui->lineEdit_y1->setValidator(new QIntValidator(1,1000,this));
    ui->lineEdit_x2->setValidator(new QIntValidator(1,1000,this));
    ui->lineEdit_y2->setValidator(new QIntValidator(1,1000,this));

    this->setWindowFlags(Qt::WindowCloseButtonHint|Qt::WindowMinimizeButtonHint);
    this->setFixedSize(this->width(),this->height());
}

ms_window::~ms_window()
{
    delete ui;
}

void ms_window::on_pushButton_no_clicked()
{
    close();
}


void ms_window::on_pushButton_yes_clicked()
{
    if(ui->lineEdit_x1->text().isEmpty() || ui->lineEdit_y1->text().isEmpty() || ui->lineEdit_x2->text().isEmpty() || ui->lineEdit_y2->text().isEmpty())
    {
        QMessageBox::warning(nullptr, "提示", "输入有误！", QMessageBox::Yes |  QMessageBox::Yes);
        return;
    }

    int x1 = ui->lineEdit_x1->text().toInt();
    int y1 = ui->lineEdit_y1->text().toInt();
    int x2 = ui->lineEdit_x2->text().toInt();
    int y2 = ui->lineEdit_y2->text().toInt();

    emit SendSignal(x1,y1,x2,y2);
    close();
}
