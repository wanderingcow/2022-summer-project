#ifndef MS_WINDOW_H
#define MS_WINDOW_H

#include <QWidget>

namespace Ui {
class ms_window;
}

class ms_window : public QWidget
{
    Q_OBJECT

public:
    explicit ms_window(QWidget *parent = nullptr);
    ~ms_window();

private slots:
    void on_pushButton_no_clicked();

    void on_pushButton_yes_clicked();

private:
    Ui::ms_window *ui;

public:
signals:
    void SendSignal(int x1,int y1,int x2,int y2);
};

#endif // MS_WINDOW_H
