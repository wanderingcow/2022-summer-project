#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include "imageclass1.1/Image.h"

QT_BEGIN_NAMESPACE
namespace Ui
{
    class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    QImage AdjustImageSize(QImage qimage, QLabel *qLabel);

private slots:
    void on_action_open_triggered();

    void on_action_save_triggered();

    void on_action_Qimage_triggered();

    void on_label_show_linkActivated(const QString &link);

private:
    Ui::MainWindow *ui;

    Image srcImage;
    QString file_path;

    bool Is_show = false;
};
#endif // MAINWINDOW_H
