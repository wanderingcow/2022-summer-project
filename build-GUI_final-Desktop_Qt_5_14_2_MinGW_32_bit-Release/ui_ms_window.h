/********************************************************************************
** Form generated from reading UI file 'ms_window.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MS_WINDOW_H
#define UI_MS_WINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ms_window
{
public:
    QPushButton *pushButton_no;
    QPushButton *pushButton_yes;
    QLineEdit *lineEdit_x1;
    QLineEdit *lineEdit_y1;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QLineEdit *lineEdit_x2;
    QLabel *label_4;
    QLineEdit *lineEdit_y2;

    void setupUi(QWidget *ms_window)
    {
        if (ms_window->objectName().isEmpty())
            ms_window->setObjectName(QString::fromUtf8("ms_window"));
        ms_window->resize(307, 88);
        pushButton_no = new QPushButton(ms_window);
        pushButton_no->setObjectName(QString::fromUtf8("pushButton_no"));
        pushButton_no->setGeometry(QRect(210, 20, 75, 24));
        pushButton_yes = new QPushButton(ms_window);
        pushButton_yes->setObjectName(QString::fromUtf8("pushButton_yes"));
        pushButton_yes->setGeometry(QRect(210, 50, 75, 24));
        lineEdit_x1 = new QLineEdit(ms_window);
        lineEdit_x1->setObjectName(QString::fromUtf8("lineEdit_x1"));
        lineEdit_x1->setGeometry(QRect(40, 20, 51, 21));
        lineEdit_y1 = new QLineEdit(ms_window);
        lineEdit_y1->setObjectName(QString::fromUtf8("lineEdit_y1"));
        lineEdit_y1->setGeometry(QRect(130, 20, 61, 21));
        label = new QLabel(ms_window);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(10, 20, 31, 16));
        label_2 = new QLabel(ms_window);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(100, 20, 31, 16));
        label_3 = new QLabel(ms_window);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(100, 50, 31, 16));
        lineEdit_x2 = new QLineEdit(ms_window);
        lineEdit_x2->setObjectName(QString::fromUtf8("lineEdit_x2"));
        lineEdit_x2->setGeometry(QRect(40, 50, 51, 21));
        label_4 = new QLabel(ms_window);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(10, 50, 31, 16));
        lineEdit_y2 = new QLineEdit(ms_window);
        lineEdit_y2->setObjectName(QString::fromUtf8("lineEdit_y2"));
        lineEdit_y2->setGeometry(QRect(130, 50, 61, 21));

        retranslateUi(ms_window);

        QMetaObject::connectSlotsByName(ms_window);
    } // setupUi

    void retranslateUi(QWidget *ms_window)
    {
        ms_window->setWindowTitle(QCoreApplication::translate("ms_window", "\351\251\254\350\265\233\345\205\213", nullptr));
        pushButton_no->setText(QCoreApplication::translate("ms_window", "\345\217\226\346\266\210", nullptr));
        pushButton_yes->setText(QCoreApplication::translate("ms_window", "\347\241\256\345\256\232", nullptr));
        label->setText(QCoreApplication::translate("ms_window", "x1:", nullptr));
        label_2->setText(QCoreApplication::translate("ms_window", "y1:", nullptr));
        label_3->setText(QCoreApplication::translate("ms_window", "y2:", nullptr));
        label_4->setText(QCoreApplication::translate("ms_window", "x2:", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ms_window: public Ui_ms_window {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MS_WINDOW_H
